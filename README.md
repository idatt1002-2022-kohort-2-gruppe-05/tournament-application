<br/>
<p align="center">
        <img width="50%" src="https://i.ibb.co/Xjw0JDF/pinpoint-logo-3.png" alt="pinpoint logo">
</p>


<h1 align="center">pinpoint - Tournament Application</h1>


<br/>

<p align="center">
    <img width="80%" src="https://i.ibb.co/2Fx1VF0/pinpoint-design.png" alt="pinpoint's design">
</p>
<br/>

# Tournament Application

Made By K2G05

## 🚀 Getting started

Pinpoint requires Maven and Java JDK17


```
# Setup a local repository
git clone https://gitlab.stud.idi.ntnu.no/idatt1002-2022-kohort-2-gruppe-05/tournament-application.git
cd tournament-application

# Run the application
mvn clean javafx:run

# Thats it!
```

#### ⚙ Configuration
To enable DEBUG mode, you need to press F12 on the first window of the application.
This only works when you press F12 the first time you are on the opening view.
## ✅ Test the application
The tests can be run with maven by running `mvn test`.

## ❤ Contributing
The Tournament Application, pinpoint's, backend is an open source project.
The project was started by 5 students at NTNU as part of their study program.
We are committed to a fully transparent development process
and highly appreciate any contributions.
Whether you are helping us fixing bugs, proposing new feature, improving our documentation
or spreading the word - **we would love to have you as part of the community**.

## 🤝  Found a bug? Missing a specific feature?
Feel free to file a new issue with a respective title and description
on the the [Tournament Application](https://gitlab.stud.idi.ntnu.no/idatt1002-2022-kohort-2-gruppe-05/tournament-application.git) repository.
If you already found a solution to your problem, we would love to review your pull request!


## 📫 Contact
Feel free to send us a message on our official email pinpoint.application.service@gmail.com.
The response time is usally between one hour and never.

## 📘 Licence
The code in this project is licensed under MIT license.
