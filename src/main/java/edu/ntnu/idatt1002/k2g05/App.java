package edu.ntnu.idatt1002.k2g05;

import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import edu.ntnu.idatt1002.k2g05.View;

import java.util.Objects;

public class App extends Application {
    private final static String TITLE = "Pinpoint";
    private final static String LOGO_PATH = "images/symbol-logo.png";
    private final static boolean RESIZEABLE = false;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        View view = View.getInstance();
        view.setStage(stage);
        view.setCurrentScene(View.OPENING_VIEW);
        view.getCurrentScene().setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.F12) {
                if (view.isDebugMode()) {
                    if (Popups.showConfirmation("Debug Mode", "Vil du deaktivere debug mode?",
                            "Ved å trykke F12 og ok deaktiverer man debug modus")) {
                        view.setDebugMode(false);
                    }

                } else {
                    if (Popups.showConfirmation("Debug Mode", "Vil du aktivere debug mode?",
                            "Ved å trykke F12 og ok aktiverer man debug modus")) {
                        view.setDebugMode(true);
                    }
                }
            }
        });
        setAppearance(stage);
        stage.show();
    }

    /**
     * Applies some estatic changes to stage, set by the static constants of the class.
     * 
     * @param stage
     *            to be applied to
     */
    private void setAppearance(Stage stage) {
        stage.setTitle(TITLE);
        stage.getIcons().add(new Image(Objects.requireNonNull(App.class.getResourceAsStream(LOGO_PATH))));
        stage.setResizable(RESIZEABLE);
    }
}
