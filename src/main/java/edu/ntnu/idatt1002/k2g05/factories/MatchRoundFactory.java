package edu.ntnu.idatt1002.k2g05.factories;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for managing logic around match round.
 */
public final class MatchRoundFactory {

    private MatchRoundFactory() {
    }

    /**
     * Method for filling in matches in a Match round given the Round Robin format.
     * 
     * @param round
     *            to be filled
     */
    public static void fillMatchesInMatchRoundWithRoundRobin(MatchRound round) {
        if (round.getAmountMatchGames() % 2 != 0) {
            throw new IllegalArgumentException("Runde må være partall");
        }

        int numberOfMatches = (round.getAmountMatchGames() - 1); // Days needed to complete tournament
        int halfSize = round.getAmountMatchGames() / 2;

        List<MatchGame> matches = new ArrayList<>(round.getAllMatchGames()); // Add teams to List and remove the first
                                                                             // team
        matches.remove(0);

        int matchesSize = matches.size();

        for (int match = 0; match < numberOfMatches; match++) {
            // Match (match + 1)

            int matchIdx = match % matchesSize;

            matches.get(matchIdx).addMatch(new Match(round.getMatchGame(0).getPlayer()));
            round.getMatchGame(0).addMatch(new Match(matches.get(matchIdx).getPlayer()));
            // matches[teamIdx] vs ListTeam[0]

            for (int idx = 1; idx < halfSize; idx++) {
                int firstMatch = (match + idx) % matchesSize;
                int secondMatch = (match + matchesSize - idx) % matchesSize;
                matches.get(firstMatch).addMatch(new Match(matches.get(secondMatch).getPlayer()));
                matches.get(secondMatch).addMatch(new Match(matches.get(firstMatch).getPlayer()));
                // matches[firstMatch] vs matches[secondMatch]
            }
        }
    }

}
