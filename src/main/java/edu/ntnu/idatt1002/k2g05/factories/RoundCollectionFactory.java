package edu.ntnu.idatt1002.k2g05.factories;

import edu.ntnu.idatt1002.k2g05.models.*;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for making round collections easier to use.
 */
public final class RoundCollectionFactory {

    private RoundCollectionFactory() {
    }

    /**
     * Method for getting and filling a qualification round collection given a nam eof the round collection and the
     * players that is going to play in the qualificatio
     * 
     * @param name
     *            of the collection
     * @param players
     *            to participate.
     * @return the RoundCollection created.
     */
    public static RoundCollection getAndFillQualificationRoundCollection(String name, List<Player> players) {
        RoundCollection collection = new RoundCollection(name, RoundType.QUALIFICATION_ROUND);
        QualificationRound round = new QualificationRound(name);
        for (Player player : players) {
            round.addGame(new Game(player));
        }
        collection.addRound(round);

        return collection;
    }

    public static RoundCollection getEmptyGroupStageCollection(String name, int groups) {

        if (groups < 1) {
            throw new IllegalArgumentException("Antall grupper kan ikke være mindre enn en.");
        }

        RoundCollection roundCollection = new RoundCollection(name, RoundType.GROUP_STAGE);
        for (int i = 0; i < groups; i++) {
            roundCollection.addRound(new MatchRound("Gruppe " + (char) (i + 65), false));
        }
        return roundCollection;
    }

    /**
     * Method for creating an empty quarter final collection given the nam
     * 
     * @param name
     *            of the collection
     * @return the empty collection
     */
    public static RoundCollection getEmptyQuarterFinalCollection(String name) {
        RoundCollection roundCollection = new RoundCollection(name, RoundType.QUARTER_FINAL);
        for (int i = 1; i <= 4; i++) {
            roundCollection.addRound(new MatchRound("QF" + i, true));
        }
        return roundCollection;
    }

    /**
     * Method for getting an empty semi-final
     * 
     * @param name
     *            of the round collection
     * @return the empty collection
     */
    public static RoundCollection getEmptySemiFinalCollection(String name) {
        RoundCollection roundCollection = new RoundCollection(name, RoundType.SEMI_FINAL);
        roundCollection.addRound(new MatchRound("SF1", true));
        roundCollection.addRound(new MatchRound("SF2", true));
        return roundCollection;
    }

    /**
     * Method for creating an empty final collection
     * 
     * @param name
     *            of the collection
     * @return the empty collection
     */
    public static RoundCollection getEmptyFinalCollection(String name) {
        RoundCollection roundCollection = new RoundCollection(name, RoundType.FINAL);
        roundCollection.addRound(new MatchRound("F1", true));
        return roundCollection;
    }

    /**
     * Method for filling in a group stage collection with snake fill given the players that are going to be filled in
     * the collection Snake fill looks like with 16 players with 4 per round this: (where number is position after
     * qualification)
     *
     * |A |B |C |D | |1 |2 |3 |4 | |8 |7 |6 |5 | |9 |10|11|12| |16|15|14|13|
     *
     * @param collection
     *            to be filled
     * @param players
     *            to put in collecition
     * @param playersPerRound
     *            amount of players per round.
     */
    public static void updateGroupStageCollectionWithSnakeFill(RoundCollection collection, List<Player> players,
            int playersPerRound) {
        if (collection.getAllRounds().size() * playersPerRound != players.size()) {
            throw new IllegalArgumentException(
                    "Number of rounds, and players per round, does not match up with amount of players");
        }

        for (Round round : collection.getAllRounds()) {
            round.clear();
        }

        List<Round> rounds = collection.getAllRounds();

        boolean reverse = false;
        for (int playerNr = 0; playerNr < players.size(); playerNr++) {
            if (reverse) {
                ((MatchRound) rounds.get(rounds.size() - (playerNr % rounds.size() + 1)))
                        .addMatchGame(new MatchGame(players.get(playerNr)));
            } else {
                ((MatchRound) rounds.get(playerNr % rounds.size())).addMatchGame(new MatchGame(players.get(playerNr)));
            }
            if (playerNr % rounds.size() >= rounds.size() - 1) {
                reverse = !reverse;
            }
        }
    }

    /**
     * Method for getting an empty qualification collection
     * 
     * @param name
     *            of collecion
     * @return empty collection
     */
    public static RoundCollection getEmptyQualificationCollection(String name) {
        RoundCollection roundCollection = new RoundCollection(name, RoundType.QUALIFICATION_ROUND);
        roundCollection.addRound(new QualificationRound("Q1"));
        return roundCollection;
    }

    /**
     * Method for filling a given qualification collection with players
     * 
     * @param collection
     * @param players
     */
    public static void fillQualificationCollection(RoundCollection collection, List<Player> players) {

        if (!(collection.getType() == RoundType.QUALIFICATION_ROUND)) {
            throw new IllegalArgumentException("rundekolleksjonen du sendte inn er ikke en kvalifikasjons runde");
        }

        QualificationRound round = (QualificationRound) collection.getAllRounds().get(0);
        for (Player player : players) {
            round.addGame(new Game(player));
        }
    }

    /**
     * Method for filling a final collection from a group play collection. The amount of group must match the degree
     * of final, E.g with 4 groups, it must go into a quarter final, with 8 groups it must go to an 8th-final.
     *
     * @param finalCollection to be filled
     * @param groupPlayCollection to fill from
     */
    public static void fillFinalFromGroupPlay(RoundCollection finalCollection, RoundCollection groupPlayCollection) {
        if(finalCollection.getAmountOfRounds() != groupPlayCollection.getAmountOfRounds()) {
            throw new IllegalArgumentException("Number of match games given must match number finalist rank");
        }

        for (int i = 0; i < groupPlayCollection.getAmountOfRounds(); i++) {
            if (!(finalCollection.getRound(i) instanceof MatchRound finalMatchRound)) {
                throw new IllegalArgumentException("Finals can only contain Match Rounds.");
            }
            if (!(groupPlayCollection.getRound(i) instanceof MatchRound groupGame)) {
                throw new IllegalArgumentException("Group Play can only contain Match Rounds.");
            }

            List<MatchGame> matchGameTop = groupGame.getTopMatchGames(2);

            Player first  = matchGameTop.get(0).getPlayer();
            Player second = matchGameTop.get(1).getPlayer();

            MatchGame firstMatchGame  = new MatchGame(first);
            MatchGame secondMatchGame = new MatchGame(second);

            firstMatchGame.addMatch(new Match(second));
            secondMatchGame.addMatch(new Match(first));

            finalMatchRound.addMatchGame(firstMatchGame);
            finalMatchRound.addMatchGame(secondMatchGame);
        }
    }

    /**
     * Method for filling the next final given the previous final, e.g. filling a semi-final by th winners of the quarter-final
     * @param finalCollection to be filled.
     * @param previousFinalCollection to fill from.
     */
    public static void fillFinalFromPreviousFinal(RoundCollection finalCollection,
            RoundCollection previousFinalCollection) {
        if (finalCollection.getAmountOfRounds() != previousFinalCollection.getAmountOfRounds() / 2) {
            throw new IllegalArgumentException("Number of matches ign given must match number finalist rank");
        }

        List<MatchGame> winners = new ArrayList<>();

        for (Round round : previousFinalCollection.getAllRounds()) {
            if (!(round instanceof MatchRound matchRound)) {
                throw new IllegalArgumentException("Previous final collection must contain match rounds ");
            }

            winners.add(matchRound.getTopMatchGames(1).get(0));
        }

        for (int i = 0; i < finalCollection.getAllRounds().size(); i++) {
            if (!(finalCollection.getRound(i) instanceof MatchRound matchRound)) {
                throw new IllegalArgumentException("Final collection must contain match rounds");
            }

            MatchGame firstMatchGame = new MatchGame(winners.get(i*2).getPlayer());
            firstMatchGame.addMatch(new Match(winners.get(i*2 + 1).getPlayer()));

            MatchGame secondMatchGame = new MatchGame(winners.get(i*2 +1).getPlayer());
            secondMatchGame.addMatch(new Match(winners.get(i*2 ).getPlayer()));

            matchRound.addMatchGame(firstMatchGame);
            matchRound.addMatchGame(secondMatchGame);
        }
    }
}
