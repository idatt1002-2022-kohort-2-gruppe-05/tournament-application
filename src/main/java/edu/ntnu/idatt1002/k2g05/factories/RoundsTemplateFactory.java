package edu.ntnu.idatt1002.k2g05.factories;

import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundsTemplate;

/**
 * Factory for creating templates for tournaments
 */
public final class RoundsTemplateFactory {

    private RoundsTemplateFactory() {
    }

    /**
     * World Championship template is consists of: - Qualification Round - A group stag - Quarter final - Semi-final -
     * Final
     *
     * @return the template
     */
    public static RoundsTemplate getWorldChampionshipsTemplate() {
        RoundsTemplate template = new RoundsTemplate();

        template.addRound(RoundType.QUALIFICATION_ROUND);
        template.addRound(RoundType.GROUP_STAGE);
        template.addRound(RoundType.QUARTER_FINAL);
        template.addRound(RoundType.SEMI_FINAL);
        template.addRound(RoundType.FINAL);

        return template;
    }
}
