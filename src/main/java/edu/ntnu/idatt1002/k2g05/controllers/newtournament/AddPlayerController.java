package edu.ntnu.idatt1002.k2g05.controllers.newtournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.debug.DebugData;
import edu.ntnu.idatt1002.k2g05.models.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * Controller for the add player view.
 */
public class AddPlayerController extends BaseNewTournamentController {

    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField country;
    @FXML
    private TextField club;
    @FXML
    private TextField license;
    @FXML
    private TextField playClass;
    @FXML
    private RadioButton male;
    @FXML
    private VBox scrollPane;
    @FXML
    private Label amountOfPlayers;
    @FXML
    private TextField searchBar;

    /**
     * Method that gets called after FXML elements are loaded.
     */
    public void initialize() {
        if (view.isDebugMode()) {
            addDebugButton();
        }
        updateScrollPane();
    }

    private void addDebugButton() {
        HBox parent = (HBox) searchBar.getParent();
        Button debugButton = new Button("Legg til test spillere.");
        debugButton.setOnAction(actionEvent -> loadTestData());
        debugButton.setStyle("-fx-background-color: red");
        parent.getChildren().add(debugButton);
    }

    /**
     * Debug method for loading 35 random players to the tournament.
     */
    private void loadTestData() {
        int number = 35;
        for (int i = 0; i < number; i++) {
            addPlayer(DebugData.getTestPlayer());
        }
    }

    /**
     * Method that gets called when the addPlayerButton is pressed. It wil retreive all the nessesary information from
     * the given text fields.
     */
    @FXML
    private void addPlayerButton() {
        try {
            char gender = male.isSelected() ? 'M' : 'F';
            String name = firstName.getText().strip() + " " + lastName.getText().strip();
            char playClass = this.playClass.getText().strip().charAt(0);
            String club = this.club.getText().strip();
            int license = Integer.parseInt(this.license.getText().strip());
            String country = this.country.getText().strip();

            Player player = new Player(name, gender, playClass, club, license, 0, country);
            addPlayer(player);
        } catch (NumberFormatException e) {
            Popups.showError("Feil ved inntasting", "Feil ved inntasting",
                    "Feilen var: \n" + "Lisensen må være et nummer");

        } catch (IllegalArgumentException e) {
            Popups.showError("Feil ved inntasting", "Feil ved inntasting", "Feilen var: \n" + e.getMessage());
        }
    }

    /**
     * Method for adding a player to the current tournament given a player
     * 
     * @param player
     *            to be added
     */
    private void addPlayer(Player player) {
        models.getCurrentTournament().addPlayer(player);
        updateScrollPane();
        clearInputFields();
    }

    /**
     * Method for updating the scroll pane with players.
     */
    @FXML
    private void updateScrollPane() {
        scrollPane.getChildren().clear();
        if (!searchBar.getText().isBlank()) {
            for (Player player : models.getCurrentTournament().getPlayersBySearch(searchBar.getText(), 30)) {
                scrollPane.getChildren().add(getPlayerCard(player));
            }
        } else {
            for (Player player : models.getCurrentTournament().getAllPlayers()) {
                scrollPane.getChildren().add(getPlayerCard(player));
            }
        }

        amountOfPlayers.setText("Lagt til: " + models.getCurrentTournament().getAmountOfPlayers());
    }

    /**
     * Method for clearing all the input fields
     */
    private void clearInputFields() {
        firstName.clear();
        lastName.clear();
        country.clear();
        club.clear();
        license.clear();
        playClass.clear();
    }

    /**
     * Method for getting a player card to be shown. A player card is a representation of a player that is going to be
     * put in the scoll pane
     * 
     * @param player
     * @return
     */
    private Node getPlayerCard(Player player) {
        String style = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 10px 5px 10px 10px;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: white;
                -fx-effect: dropshadow(three-pass-box , rgba(0,0,0,0.6) , 5, 0, 0, 1);
                """;
        HBox card = new HBox();
        Label name = new Label(player.getName() + ", " + player.getLicense() + ", " + player.getClub());
        Region spacer = new Region();
        Button remove = new Button("Fjern");

        remove.setStyle("-fx-background-color: -color-dark");
        remove.setOnAction(actionEvent -> {
            models.getCurrentTournament().removePlayer(player);
            updateScrollPane();
        });


        card.getChildren().add(name);
        card.getChildren().add(spacer);
        card.getChildren().add(remove);
        card.setStyle(style);
        name.setMinWidth(Region.USE_PREF_SIZE);
        remove.setMinWidth(70);
        spacer.setPrefWidth(1000);
        card.setMinHeight(50);

        return card;
    }

    @FXML
    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_2);
    }

    @FXML
    @Override
    void onNextPress(ActionEvent event) {
        if (models.getCurrentTournament().getAmountOfPlayers() < 32) {
            Popups.showError("Feil", "Ikke nok spillere",
                    "Det må være over 32 spillere for å gå" + " videre til kvalifikasjon");
        } else {
            view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_4);
        }
    }
}
