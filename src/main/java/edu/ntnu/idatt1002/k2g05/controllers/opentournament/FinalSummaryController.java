package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.*;

/**
 * Controller for the final summery FXML
 */
public class FinalSummaryController extends BaseOpenTournamentController {

    String roundStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 10px 5px 10px 10px;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: white;
                -fx-effect: dropshadow(three-pass-box , rgba(0,0,0,0.6) , 5, 0, 0, 1);
            """;

    List<Node> scrollPaneContent = new ArrayList<>();

    @FXML
    private VBox scrollPane;

    /**
     * Method that gets called after FXML elements are loaded.
     */
    public void initialize() {
        models.autoSave();
        updateScrollPane();
    }

    /**
     * Method that updates the with new content scollpane
     */
    private void updateScrollPane() {
        updateScrollPaneContent();
        scrollPane.getChildren().clear();

        for (Node node : scrollPaneContent) {
            scrollPane.getChildren().add(node);
        }
    }

    /**
     * Method that updates the content of the scrollPaneContent list.
     */
    private void updateScrollPaneContent() {
        RoundsTemplate template = models.getCurrentTournament().getRoundsTemplate();
        List<RoundType> roundTypesCopy = new ArrayList<>(template.getRounds());

        Collections.reverse(roundTypesCopy);
        for (RoundType type : roundTypesCopy) {
            try {
                RoundCollection collection = models.getCurrentTournament().getRoundCollections(type).orElseThrow();
                scrollPaneContent.add(getCollectionCard(collection));
            } catch (NoSuchElementException e) {
                Popups.showError("Feil", "Noe feil skjedde med innlastingen av runder.", e.getMessage());
            }
        }
    }

    /**
     * Creates a card for collection
     * 
     * @param collection
     *            to create from
     * @return card
     */
    private Node getCollectionCard(RoundCollection collection) {
        Node card = null;
        switch (collection.getType()) {
        case QUALIFICATION_ROUND -> card = getQualificationSummaryNode(collection);
        case GROUP_STAGE -> card = getGroupStageSummaryNode(collection);
        case QUARTER_FINAL, SEMI_FINAL, FINAL -> card = getFinalSummaryNode(collection);
        }
        return card;
    }

    /**
     * Method for getting a headers card, by its columns and spacing in percent
     * @param columns names
     * @param spacing in percent
     * @return header card
     */
    private Node getHeaderCard(String[] columns, int[] spacing) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 6 20 6 20;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: -color-dark;
                """;
        GridPane header = new GridPane();
        header.getColumnConstraints().addAll(getColumnConstraints(spacing));

        for (int i = 0; i < columns.length; i++) {
            Label columnName = new Label(columns[i]);
            columnName.setStyle("-fx-text-fill: white;");
            header.addColumn(i, columnName);
        }
        header.setStyle(cardStyle);
        return header;
    }

    /**
     * Get column constaints given array of spacings.
     * 
     * @param spacing
     *            to be created from
     * @return list og column constraints
     */
    private List<ColumnConstraints> getColumnConstraints(int[] spacing) {
        List<ColumnConstraints> constraints = new ArrayList<>();
        for (int space : spacing) {
            ColumnConstraints con = new ColumnConstraints();
            con.setPercentWidth(space);
            constraints.add(con);
        }
        return constraints;
    }

    /**
     * Method for getting a group stage Summary card/node
     * @param collection to create from.
     * @return
     */
    private Node getGroupStageSummaryNode(RoundCollection collection) {
        int[] spacing = new int[] { 25, 22, 10, 10, 10, 10, 13 };
        String[] headers = new String[] { "Spiller", "Land", "Spilt", "Vunnet", "Uavgjordt", "Tapt", "Poeng" };

        Label label = new Label(collection.getName());
        VBox card = new VBox(label, getHeaderCard(headers, spacing));

        for (Round round : collection.getAllRounds()) {
            if (round instanceof MatchRound matchRound) {
                card.getChildren().add(getGroupRoundCard(spacing, matchRound.getAllMatchGames()));
            }
        }

        return card;
    }

    /**
     * Method for getting a group card given spacing and matchGames
     * 
     * @param spacing
     *            as an array of percent values to define space take by each index
     * @param matchGames
     *            to be shown in the card
     * @return the round card.
     */
    private Node getGroupRoundCard(int[] spacing, List<MatchGame> matchGames) {
        GridPane playersCard = new GridPane();
        playersCard.getColumnConstraints().addAll(getColumnConstraints(spacing));

        for (int i = 0; i < matchGames.size(); i++) {
            MatchGame matchGame = matchGames.get(i);
            Player player = matchGame.getPlayer();

            Label name = new Label(player.getName());
            Label country = new Label(player.getCountry());
            Label played = new Label(Integer.toString(matchGame.getPlayed()));
            Label won = new Label(Integer.toString(matchGame.getWon()));
            Label draws = new Label(Integer.toString(matchGame.getDraw()));
            Label lost = new Label(Integer.toString(matchGame.getLost()));
            Label points = new Label(Integer.toString(matchGame.getPoints()));

            playersCard.addRow(i, name, country, played, won, draws, lost, points);
        }
        playersCard.setStyle(roundStyle);
        return playersCard;
    }

    /**
     * Method for getting the summary cards for finals.
     * @param collection final
     * @return the card/node
     */
    private Node getFinalSummaryNode(RoundCollection collection) {
        String[] headers = new String[]{"Spiller", "Land", "Resultat", "poeng", "Vinner"};
        int[] spacing = new int[]{25, 25, 10, 15, 25};

        Label label = new Label(collection.getName());
        VBox card = new VBox(label, getHeaderCard(headers, spacing));


        for (Round round : collection.getAllRounds()) {
            if (round instanceof MatchRound matchRound) {
                card.getChildren().add(getFinalRoundCard(matchRound, spacing));

            }
        }

        return card;
    }

    /**
     * Method for getting the data from match-round given spacing
     * 
     * @param matchRound
     *            to be displayed
     * @param spacing
     *            as an array of percent values to define space take by each index
     * @return the card/node
     */
    private Node getFinalRoundCard(MatchRound matchRound, int[] spacing) {
        GridPane playersCard = new GridPane();
        playersCard.getColumnConstraints().addAll(getColumnConstraints(spacing));
        matchRound.sort(SortType.PLACEMENT, true);

        for (int i = 0; i < matchRound.getAllMatchGames().size(); i++) {
            MatchGame matchGame = matchRound.getAllMatchGames().get(i);
            Player player = matchGame.getPlayer();

            Label name = new Label(player.getName());
            Label country = new Label(player.getCountry());
            Label points = new Label(Integer.toString(matchGame.getAllMatches().get(0).getPlayerScore()));
            Label score = new Label(Integer.toString(matchGame.getPoints()));

            playersCard.addRow(i, name, country, points, score);
        }

        Player winner = matchRound.getMatchGame(0).getPlayer();

        playersCard.add(new Label(winner.getName()), 4, 0, 1, 2);
        playersCard.setStyle(roundStyle);
        return playersCard;
    }

    /**
     * Method for getting the qualification summary node
     * @param collection qualifier
     * @return node/card
     */
    private Node getQualificationSummaryNode(RoundCollection collection) {
        String[] headers = new String[]{"Place", "Spiller", "Land", "", "Total", "Gjenomsnitt", "Diff"};
        int[] spacing = new int[]{5, 25, 20, 20, 10,10, 10};

        Label label = new Label(collection.getName());

        VBox games = new VBox();

        for (Round round : collection.getAllRounds()) {
            if (round instanceof QualificationRound qualificationRoundRound) {
                qualificationRoundRound.sort(SortType.PLACEMENT, true);
                int placement = 1;
                Game firstGame = qualificationRoundRound.getAllGames().get(0);
                for (Game game : qualificationRoundRound.getAllGames()) {
                    games.getChildren().add(getQualificationGameCard(firstGame, game, placement, spacing));
                    placement++;
                }
            }
        }
        games.setStyle(roundStyle);

        VBox card = new VBox(label, getHeaderCard(headers, spacing), games);

        return card;
    }

    /**
     * Method for getting the Qualification card data displayed
     * 
     * @param firstGame
     *            top game to measure diff from
     * @param game
     *            current game to get display from
     * @param placement
     *            of th game
     * @param spacing
     *            of the nodes columns
     * @return the node
     */
    private Node getQualificationGameCard(Game firstGame, Game game, int placement, int[] spacing) {
        GridPane grid = new GridPane();
        grid.getColumnConstraints().addAll(getColumnConstraints(spacing));

        Label place = new Label(placement + ". ");
        Label player = new Label(game.getPlayer().getName());
        Label country = new Label(game.getPlayer().getCountry());
        Label space = new Label("");
        Label total = new Label(Integer.toString(game.getSum()));
        Label mean = new Label(String.format("%.2f", game.getMean()));
        Label diff = new Label(Integer.toString(game.getSum() - firstGame.getSum()));

        grid.addRow(0, place, player, country, space, total, mean, diff);
        return grid;
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_FINALS_REGISTRATION);
    }

    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPENING_VIEW);

    }
}
