package edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import javafx.scene.Node;

import java.util.Collection;
import java.util.HashMap;
import java.util.NoSuchElementException;

/**
 * A Match Column is related to matches between to Players. The reason for this i because of duplicates and handling
 * them accordingly
 */
public class MatchColumn {
    private final HashMap<PlayerPair, Node> matchUpNodes;
    private final HashMap<PlayerPair, Match> matchUpMatches;
    private final HashMap<PlayerPair, MatchPair> matchUpMatchPairs;

    private final int index;

    /**
     * Constructor for making an instance of the MatchColumn.
     * 
     * @param col
     *            index of the column
     */
    public MatchColumn(int col) {
        this.index = col;
        matchUpNodes = new HashMap<>();
        matchUpMatches = new HashMap<>();
        matchUpMatchPairs = new HashMap<>();
    }

    /**
     * Check if a player is in a Match Column
     * 
     * @param player
     *            to check for
     * @return
     */
    public boolean containsPlayer(Player player) {
        return matchUpNodes.keySet().stream().anyMatch(playerPair -> playerPair.containsPlayer(player));
    }

    /**
     * Method for adding a Player Pair and a correlated match.
     * 
     * @param playerPair
     *            pair to add.
     * @param match
     *            to add.
     */
    public void addPlayerPairAndMatch(PlayerPair playerPair, Match match) {
        matchUpMatches.put(playerPair, match);
    }

    /**
     * Method for adding a Player and a correlated node.
     * 
     * @param playerPair
     *            pair to add
     * @param node
     *            node to add
     */
    public void addPlayerPairAndNode(PlayerPair playerPair, Node node) {
        matchUpNodes.put(playerPair, node);
    }

    /**
     * Method for creating a match pair given a player pair and a match. The match will be combined with the previous
     * added match One should check if the player pair is already in the match column
     * 
     * @param playerPair
     *            to add
     * @param match
     *            to pair with other
     */
    public void createMatchPair(PlayerPair playerPair, Match match) {
        if (!matchUpMatches.containsKey(playerPair)) {
            throw new NoSuchElementException("No corresponding match");
        }
        Match otherMatch = matchUpMatches.get(playerPair);
        matchUpMatchPairs.put(playerPair, new MatchPair(match, otherMatch));
    }

    /**
     * Method for getting all the nodes in the match colum
     * 
     * @return the nodes
     */
    public Collection<Node> getAllNodes() {
        return matchUpNodes.values();
    }

    /**
     * Method for getting the column index
     * 
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Checks if the Match Column contains a given player pair
     * 
     * @param pair
     *            to check for
     * @return true if yes
     */
    public boolean containsPlayerPair(PlayerPair pair) {
        return matchUpMatches.keySet().stream().anyMatch(playerPair -> playerPair.equals(pair));
    }

    /**
     * Method for getting the correlating MatchPair given the player pair
     * 
     * @param pair
     *            to get the match form
     * @return the MatchPair
     * @throws NoSuchElementException
     *             if the PlayerPair is not in the MatchColumn
     */
    public MatchPair getMatchPairByPlayerPair(PlayerPair pair) {
        if (!matchUpMatchPairs.containsKey(pair)) {
            throw new NoSuchElementException("Player pair is not in list");
        }
        return matchUpMatchPairs.get(pair);
    }

    /**
     * Method for getting all the Player Pairs.
     * 
     * @return
     */
    public Collection<PlayerPair> getAllPlayerPairs() {
        return matchUpMatchPairs.keySet();
    }
}
