package edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.games.Match;

import java.util.NoSuchElementException;

/**
 * Record for holding a match pair. The reson for heaving a match pair is because of duplicates in the program. Say when a
 * player "A" plays against another player "B", player "B" will also have a Match against player "A"
 */
public record MatchPair(Match m1, Match m2) {

    /**
     * Method for checking if a given match is in a pair.
     * @param match to check with
     * @return true if match is in the pair
     */
    public boolean matchInPair(Match match) {
        return m1.equals(match) || m2.equals(match);
    }

    /**
     * Method for getting the other match in the match pair given a match. You should check if a match is in the pair before doing this.
     * @param match to get th other of
     * @return the other match
     * @throws NoSuchElementException if the match is not in the pair.
     */
    public Match getOtherMatch(Match match) {
        if (m1.equals(match)) {
            return m2;
        } else if (m2.equals(match)) {
            return m1;
        } else {
            throw new NoSuchElementException("Match not in pair");
        }
    }

    /**
     * Method for getting a match given the one of the players in the pair, one should check if the player is in the list before doing this.
     * @param player to find match with
     * @return the match
     * @throws NoSuchElementException if there is no match give the player.
     */
    public Match getMatchByPlayer(Player player) {
        if (m1.getOpponent().equals(player)) {
            return m2;
        } else if (m2.getOpponent().equals(player)) {
            return m1;
        } else {
            throw new NoSuchElementException("No match which corresponds to player");
        }
    }
}
