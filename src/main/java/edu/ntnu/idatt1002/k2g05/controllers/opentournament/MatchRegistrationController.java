package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.Group;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.MatchColumn;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.MatchPair;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.PlayerPair;
import edu.ntnu.idatt1002.k2g05.factories.MatchRoundFactory;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

import java.util.*;

/**
 * Controller class for creating
 */
public class MatchRegistrationController extends BaseOpenTournamentController {

    private final RoundCollection groupStage = models.getCurrentTournament().getRoundCollections(RoundType.GROUP_STAGE)
            .orElseThrow();
    private final List<Group> groups = new ArrayList<>();

    @FXML
    private Accordion accordion;

    @FXML
    private HBox navigation;

    /**
     * Method that gets initialised after FXML elemts are loaded.
     */
    public void initialize() {
        models.autoSave();
        updateAccordion();

        if (view.isDebugMode()) {
            addDebugButton();
        }
    }

    /**
     * Method for adding a debug button
     */
    private void addDebugButton() {
        Button debugButton = new Button("Legg til test spill.");
        debugButton.setOnAction(actionEvent -> loadTestData());
        debugButton.setStyle("-fx-background-color: red");
        navigation.getChildren().add(debugButton);
    }

    /**
     * Method for updating.
     */
    public void updateAccordion() {
        updateGroups();

        accordion.getPanes().clear();
        for (Group group : groups) {
            accordion.getPanes().add(getGroupPane(group));
        }
    }

    /**
     * Method for updateing the groups
     */
    private void updateGroups() {
        for (Round round : groupStage.getAllRounds()) {
            if (!(round instanceof MatchRound matchRound)) {
                throw new IllegalArgumentException("Group stage must hold match rounds.");
            }

            if (matchRound.getAllMatchGames().stream().map(MatchGame::getAllMatches).toList().get(0).isEmpty()) {
                MatchRoundFactory.fillMatchesInMatchRoundWithRoundRobin(matchRound);
            }

            Group group = new Group(round.getName());

            int columns = matchRound.getAllMatchGames().size() - 1;

            for (int col = 0; col < columns; col++) {
                MatchColumn matchColumn = new MatchColumn(col);
                for (MatchGame matchGame : matchRound.getAllMatchGames()) {
                    Match match = matchGame.getAllMatches().get(col);
                    PlayerPair pair = getPlayerPair(matchGame, match);

                    if (matchColumn.containsPlayerPair(pair)) {
                        matchColumn.createMatchPair(pair, match);
                    } else {
                        matchColumn.addPlayerPairAndMatch(pair, match);
                    }
                }

                for (PlayerPair pair : matchColumn.getAllPlayerPairs()) {
                    MatchPair matchPair = matchColumn.getMatchPairByPlayerPair(pair);
                    matchColumn.addPlayerPairAndNode(pair, getPlayerPairNode(pair, matchPair));
                }
                group.addMatchColumn(matchColumn);
            }

            groups.add(group);
        }
    }

    /**
     * Method for getting a node from a match and player pair
     * @param pair pair of players
     * @param matchPair pair of matches
     * @return the Node/Card
     */
    private Node getPlayerPairNode(PlayerPair pair, MatchPair matchPair) {
        HBox matchPane = new HBox();

        HBox player1Pane = new HBox();
        HBox player2Pane = new HBox();

        Player player1 = pair.p1();
        Player player2 = pair.p2();

        Match match1 = matchPair.getMatchByPlayer(player1);
        Match match2 = matchPair.getMatchByPlayer(player2);

        Label name1 = new Label(player1.getInitials());
        name1.setTooltip(new Tooltip(player1.getName()));

        Label name2 = new Label(player2.getInitials());
        name2.setTooltip(new Tooltip(player2.getName()));

        TextField score1 = new TextField();
        score1.setOnKeyTyped(a -> {
            if (!score1.getText().isBlank()) {
                try {
                    match1.setPlayerScore(Integer.parseInt(score1.getText()));
                    match2.setOpponentScore(Integer.parseInt(score1.getText()));
                } catch (NumberFormatException e) {
                    Popups.showError("Feil", "Noe Gikk galt:", e.getMessage());
                    score1.clear();
                }
            }
        });
        TextField score2 = new TextField();
        score2.setOnKeyTyped(a -> {
            if (!score2.getText().isBlank()) {
                try {
                    match1.setOpponentScore(Integer.parseInt(score2.getText()));
                    match2.setPlayerScore(Integer.parseInt(score2.getText()));
                } catch (NumberFormatException e) {
                    Popups.showError("Feil", "Noe Gikk galt:", e.getMessage());
                    score2.clear();
                }
            }
        });

        if (match1.getPlayerScore() != -1) {
            score1.setText(Integer.toString(match1.getPlayerScore()));
        }

        if (match2.getPlayerScore() != -1) {
            score2.setText(Integer.toString(match2.getPlayerScore()));
        }

        String textFieldStyle = """
                        -fx-background-color: -color-dark;
                        -fx-text-fill: white;
                        -fx-padding: 10px 10px 5px 10px;
                        -fx-border-insets: 3px 2px 0px 2px;
                        -fx-background-insets: 3px 2px 0px 2px;
                    """;

        score1.setPrefWidth(80);
        score2.setPrefWidth(80);
        score1.setStyle(textFieldStyle);
        score2.setStyle(textFieldStyle);
        name1.setPrefWidth(60);
        name2.setPrefWidth(60);
        HBox.setHgrow(name1, Priority.ALWAYS);

        player1Pane.getChildren().addAll(name1, score1);
        player2Pane.getChildren().addAll(score2, name2);

        matchPane.getChildren().addAll(player1Pane, player2Pane);

        return matchPane;
    }

    /**
     * Method for getting a Player Pair
     * 
     * @param matchGame
     * @param match
     * @return
     */
    private PlayerPair getPlayerPair(MatchGame matchGame, Match match) {
        return new PlayerPair(matchGame.getPlayer(), match.getOpponent());
    }

    /**
     * Method for getting a group pane
     * 
     * @param group
     *            pane to display
     * @return title pane
     */
    private TitledPane getGroupPane(Group group) {
        GridPane gridPane = new GridPane();

        for (MatchColumn matchColumn : group.getMatchColumns()) {
            Label title = new Label("Match " + (matchColumn.getIndex() + 1));
            GridPane.setHalignment(title, HPos.CENTER);
            gridPane.add(title, matchColumn.getIndex(), 0);

            int row = 1;
            for (Node node : matchColumn.getAllNodes()) {
                gridPane.add(node, matchColumn.getIndex(), row);
                row++;
            }
        }

        ScrollPane scrollPane = new ScrollPane(gridPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        TitledPane titledPane = new TitledPane();
        titledPane.setText(group.getName());
        titledPane.setContent(scrollPane);

        return titledPane;
    }

    /**
     * Method for loading test data.
     */
    @FXML
    private void loadTestData() {
        Random random = new Random();
        Set<Node> textFields = accordion.lookupAll("TextField");
        for (Node node : textFields) {
            ((TextField) node).setText(Integer.toString(random.nextInt(300)));
            node.fireEvent(new KeyEvent(node, node, KeyEvent.KEY_TYPED, "", "", KeyCode.UNDEFINED, false, false, false,
                    false));
        }
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_SCORE_SUMMARY);
    }

    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_MATCH_SUMMARY);
    }
}
