package edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A Group is a group in a group play but can also be used in finals. It hols the columns which are going to be filled
 * with nodes
 */
public class Group {
    private final String name;
    private final List<MatchColumn> matchColumns;

    /**
     * Constructor for creating a new instance of a group
     * 
     * @param name
     *            of the group
     */
    public Group(String name) {
        this.name = name;
        matchColumns = new ArrayList<>();
    }

    /**
     * Gets the name of the group
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets all the matchColumns in the given group
     * 
     * @return
     */
    public List<MatchColumn> getMatchColumns() {
        return matchColumns;
    }

    /**
     * Method for adding a match column to the group.
     * 
     * @param matchColumn
     *            to be added
     * @return true if the list changed.
     */
    public boolean addMatchColumn(MatchColumn matchColumn) {
        return matchColumns.add(matchColumn);
    }
}
