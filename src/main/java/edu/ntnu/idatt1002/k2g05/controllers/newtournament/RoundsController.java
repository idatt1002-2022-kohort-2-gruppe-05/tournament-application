package edu.ntnu.idatt1002.k2g05.controllers.newtournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Controller for the newTournament/rounds controller.
 */
public class RoundsController extends BaseNewTournamentController {

    private final LinkedHashMap<Node, RoundCollection> roundCollectionCards = new LinkedHashMap<>();
    private RoundCollection chosen;
    private int chosenNr = 0;

    @FXML
    private HBox scrollPane;

    @FXML
    private GridPane settingsPane;

    @FXML
    private ScrollPane parentPane;

    @FXML
    private Label collection;

    @FXML
    private TextField rounds;

    @FXML
    private TextField playersPerRound;

    @FXML
    private TextField series;

    /**
     * Method that gets called after FXML elements are loaded.
     */
    public void initialize() {
        updateSettingsPane();
        updateScrollPane();
        parentPane.setFitToHeight(true);
        parentPane.setFitToWidth(true);
    }

    /**
     * When the add round button is pressed.
     */
    @FXML
    void onAddRoundButton() {
        Popups.showError("Støtte", "Denne funksjonaliteten er ikke støttet enda.", "");
    }

    /**
     * Method for updating the settings pane on the right side of the scene.
     */
    private void updateSettingsPane() {
        chosen = models.getCurrentTournament().getAllRoundCollections().get(chosenNr);
        collection.setText(chosen.getName());
        rounds.setText(Integer.toString(chosen.getAmountOfRounds()));

        String playerPerLabel = "";
        String seriesLabel = "";

        // Due to time limitations this is hard coded to just be for show
        switch (chosen.getType()) {
        case QUALIFICATION_ROUND -> {
            ;
            playerPerLabel = Integer.toString(models.getCurrentTournament().getAmountOfPlayers());
            seriesLabel = "6";
        }
        case GROUP_STAGE -> {
            playerPerLabel = "8";
            seriesLabel = "7";
        }

        case FINAL, SEMI_FINAL, QUARTER_FINAL -> {
            playerPerLabel = "2";
            seriesLabel = "1";
        }
        }

        playersPerRound.setText(playerPerLabel);
        series.setText(seriesLabel);

    }

    /**
     * Method for updateing the scroll pane with the rounds.
     */
    private void updateScrollPane() {
        updateRoundCollectionCards();
        scrollPane.getChildren().clear();
        for (Node container : roundCollectionCards.keySet()) {
            scrollPane.getChildren().add(container);
        }
    }

    /**
     * Method for updating all the round Collection cards. This will clear the previous round collection cards. And
     * create new ones
     */
    private void updateRoundCollectionCards() {
        List<RoundCollection> roundCollections = models.getCurrentTournament().getAllRoundCollections();

        roundCollectionCards.clear();
        for (RoundCollection roundCollection : roundCollections) {
            VBox outerContainer = getRoundsContainer(roundCollection.getName());
            VBox container = (VBox) outerContainer.getChildren().get(1);

            switch (roundCollection.getType()) {
            case QUALIFICATION_ROUND -> fillQualification(container);
            case GROUP_STAGE -> fillGroupPlay(container, roundCollection.getAmountOfRounds(), 8);
            case QUARTER_FINAL -> fillFinal(container, 4);
            case SEMI_FINAL -> fillFinal(container, 2);
            case FINAL -> fillFinal(container, 1);
            }

            if (roundCollection == chosen) {
                for (Node child : container.getChildren()) {
                    child.setStyle(child.getStyle() + "-fx-background-color: -color-green;");
                }
            }

            roundCollectionCards.put(outerContainer, roundCollection);
        }
    }

    /**
     * Method for getting the container that holds the Rounds
     * 
     * @param name
     *            title of rounds box
     * @return container
     */
    private VBox getRoundsContainer(String name) {
        VBox box = new VBox();

        Label title = new Label(name);
        VBox rounds = new VBox();

        box.getChildren().addAll(title, rounds);
        return box;
    }

    /**
     * Method for filling a container with the qualification round.
     * 
     * @param container
     *            to be filled
     */
    private void fillQualification(VBox container) {
        container.getChildren().add(getRoundCard(models.getCurrentTournament().getAmountOfPlayers(), 400));
    }

    /**
     * Method for filling a container with the groupPlay round.
     * 
     * @param container
     *            to be fileld
     * @param rounds
     *            amount of rounds
     * @param playerPerRound
     *            players per round
     */
    private void fillGroupPlay(VBox container, int rounds, int playerPerRound) {
        for (int round = 0; round < rounds; round++) {
            container.getChildren()
                    .add(getRoundCard(playerPerRound, (int) (container.getPrefHeight() / (rounds * 1.5))));
        }
    }

    /**
     * Method for filling a container with the final round. final round can be semi final if rounds is set to 2.
     * 
     * @param container
     *            to be filled
     * @param rounds
     *            in the final
     */
    private void fillFinal(VBox container, int rounds) {
        for (int round = 0; round < rounds; round++) {
            container.getChildren().add(getRoundCard(2, (int) (container.getPrefHeight() / (rounds * 2))));
        }
    }

    /**
     * Method for getting round card node for representing a round.
     * @param amount of players
     * @param height
     * @return
     */
    private Node getRoundCard(int amount, int height) {
        String cardStyle = """
                -fx-display: inline-block;
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-background-color: -color-dark;
                -fx-border-width: 5px;
                -fx-border-insets: 4px;
                -fx-border-color: transparent;
                """;

        VBox box = new VBox();
        Label name = new Label("Spillere: \n" + amount);

        name.setStyle("-fx-text-fill: white; -fx-text-alignment: center;");
        box.getChildren().add(name);
        box.setStyle(cardStyle);
        box.setMinHeight(height);
        return box;

    }

    /**
     * Method to be called when user presses the next button for rounds
     */
    @FXML
    void nextCollection() {
        if (roundCollectionCards.size() > chosenNr + 1) {
            chosenNr++;
        } else {
            chosenNr = 0;
        }

        updateSettingsPane();
        updateScrollPane();
    }

    /**
     * Method to be called when user presses the back button for rounds
     */
    @FXML
    void backCollection() {
        if (chosenNr > 0) {
            chosenNr--;
        } else {
            chosenNr = roundCollectionCards.size() - 1;
        }

        updateSettingsPane();
        updateScrollPane();
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_3);
    }

    @Override
    void onNextPress(ActionEvent event) {
        if (Popups.showConfirmation("Bekreftelse", "Er du sikker på at du vil fortsette?",
                "Du kan ikke gå tilbake fra dette skrittet.")) {

            if (models.getCurrentTournament().getRoundCollections(RoundType.QUALIFICATION_ROUND).isPresent()) {
                RoundCollectionFactory.fillQualificationCollection(
                        models.getCurrentTournament().getRoundCollections(RoundType.QUALIFICATION_ROUND).get(),
                        models.getCurrentTournament().getAllPlayers().stream().toList());
            }

            view.setCurrentScene(View.OPEN_TOURNAMENT_SCORE_REGISTRATION);
            models.autoSave();
        }
    }
}
