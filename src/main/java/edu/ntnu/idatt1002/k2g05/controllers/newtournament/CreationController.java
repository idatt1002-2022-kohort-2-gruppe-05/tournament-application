package edu.ntnu.idatt1002.k2g05.controllers.newtournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.models.League;
import edu.ntnu.idatt1002.k2g05.models.Tournament;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.time.LocalDate;

public class CreationController extends BaseNewTournamentController {

    @FXML
    private TextField enterTournamentName;
    @FXML
    private TextField enterTournamentSaveLocation;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField enterCity;
    @FXML
    private TextField enterBowlingAlley;
    @FXML
    private RadioButton maleCheckBox;
    @FXML
    private RadioButton femaleCheckBox;
    @FXML
    private RadioButton juniorCheckBox;

    /**
     * Method that gets called after FXML elements are loaded.
     */
    public void initialize() {
        datePicker.setValue(LocalDate.now());

    }

    /**
     * Method that gets called when we are going to choose the save location.
     */
    @FXML
    private void filePickerPress() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Tournament Files (*.trnm)", "*.trnm");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(view.getStage());

        if (file != null) {
            enterTournamentSaveLocation.setText(file.getAbsolutePath());
        }
    }

    @FXML
    @Override
    protected void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPENING_VIEW);
    }

    @FXML
    @Override
    protected void onNextPress(ActionEvent event) {
        boolean cancel = false;
        if (models.getCurrentTournament() == null) {
            createTournament();
        } else {
            switch (Popups.showWarningOfOverriding("Advarsel!", "Vil du overskrive tidligere lagd turnering?",
                    "Dette vil slette alt du har gjordt på de neste sidene")) {
            case YES -> createTournament();
            case NO -> cancel = true;
            }
        }
        try {
            models.getCurrentTournament().save(enterTournamentSaveLocation.getText());
        } catch (IllegalArgumentException e) {
            Popups.showError("Feil", "Feil ved lagring", e.getMessage());
            return;
        }
        if (!cancel) {
            view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_2);
        }

    }

    /**
     * Method for creating a tournament, fetches its necessary data from text-fields.
     */
    private void createTournament() {
        String tournamentName = enterTournamentName.getText().strip();
        String city = enterCity.getText().strip();
        String bowlingAlley = enterBowlingAlley.getText().strip();

        League league = null;
        if (maleCheckBox.isSelected()) {
            league = League.MALE;
        } else if (femaleCheckBox.isSelected()) {
            league = League.FEMALE;
        } else if (juniorCheckBox.isSelected()) {
            league = League.JUNIOR;
        }

        try {
            Tournament tournament = new Tournament(tournamentName, league, datePicker.getValue(), city, bowlingAlley);
            models.setCurrentTournament(tournament);
        } catch (IllegalArgumentException | NullPointerException e) {
            Popups.showError("Feil ved inntasting", "Feil ved innstasting", "Feilen var: \n" + e.getMessage());
        }
    }
}