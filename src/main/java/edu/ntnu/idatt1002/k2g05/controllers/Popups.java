package edu.ntnu.idatt1002.k2g05.controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

/**
 * Class for showing different types of popups. e.g. Error, Confirmation and Warning popups/dialogs
 */
public class Popups {

    /**
     * Method for showing an error alert.
     * 
     * @param title
     *            of the alert
     * @param header
     *            for the alert
     * @param content
     *            for the alert
     */
    public static void showError(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.show();
    }

    /**
     * Method for showing an confirmation alert.
     * 
     * @param title
     *            of the alert
     * @param header
     *            for the alert
     * @param content
     *            for the alert
     *
     * @return true if OK, button is pressed.
     */
    public static boolean showConfirmation(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        return alert.showAndWait().filter(ButtonType.OK::equals).isPresent();
    }

    /**
     * Shows a warning pane
     *
     * @param title
     *            of warning
     * @param header
     *            of warning
     * @param content
     *            of warning
     * @return YES if override, NO if cancel and CANCEL_CLOSE if delte changes
     */
    public static ButtonBar.ButtonData showWarningOfOverriding(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        ButtonType yes = new ButtonType("Ja", ButtonBar.ButtonData.YES);
        ButtonType deleteChanges = new ButtonType("Forkast Endringer", ButtonBar.ButtonData.CANCEL_CLOSE);
        ButtonType goBack = new ButtonType("Tilbake", ButtonBar.ButtonData.NO);

        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(yes, deleteChanges, goBack);

        return alert.showAndWait().get().getButtonData();
    }
}
