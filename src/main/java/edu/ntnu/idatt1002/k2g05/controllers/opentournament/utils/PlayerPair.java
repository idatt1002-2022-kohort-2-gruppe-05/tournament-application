package edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils;

import edu.ntnu.idatt1002.k2g05.models.Player;

import java.util.NoSuchElementException;

/**
 * Record that hold two players, reson for this is dicussed in {@link MatchPair}
 */
public record PlayerPair(Player p1, Player p2) {

    /**
     * Checks if a Player pair contains a given player.
     * @param player to check with.
     * @return true if it contains a the player.
     */
    public boolean containsPlayer(Player player) {
        return p1.equals(player) || p2.equals(player);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PlayerPair that = (PlayerPair) o;

        return (p1 == that.p1() && p2 == that.p2()) || (p1 == that.p2() && p2 == that.p1());
    }

    @Override
    public int hashCode() {
        return p1.hashCode() + p2.hashCode();
    }
}
