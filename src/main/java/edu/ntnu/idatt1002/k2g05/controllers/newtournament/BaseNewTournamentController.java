package edu.ntnu.idatt1002.k2g05.controllers.newtournament;

import edu.ntnu.idatt1002.k2g05.Models;
import edu.ntnu.idatt1002.k2g05.View;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Basic new tournament controller to extend from for easier creation of new controllers.
 */
public abstract class BaseNewTournamentController {
    View view = View.getInstance();
    Models models = Models.getInstance();

    /**
     * Method that gets called when back button is pressed
     * 
     * @param event
     *            action event
     */
    @FXML
    abstract void onBackPress(ActionEvent event);

    /**
     * Method that gets called when back button is pressed
     * 
     * @param event
     *            action event
     */
    @FXML
    abstract void onNextPress(ActionEvent event);
}
