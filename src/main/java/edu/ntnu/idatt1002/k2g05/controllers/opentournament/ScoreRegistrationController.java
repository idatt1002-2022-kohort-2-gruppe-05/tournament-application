package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import edu.ntnu.idatt1002.k2g05.models.rounds.SortType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Class for Qualifying round
 */
public class ScoreRegistrationController extends BaseOpenTournamentController {

    private final HashMap<Node, Game> gameCards = new HashMap<>();
    private final QualificationRound round = (QualificationRound) models.getCurrentTournament()
            .getRoundCollections(RoundType.QUALIFICATION_ROUND).orElseThrow().getAllRounds().get(0);
    private SortType currentSortType = SortType.PLAYER;

    @FXML
    private VBox scrollPane;

    @FXML
    private TextField searchBar;

    /**
     * Method that gets called after FXML instantiation.
     */
    public void initialize() {
        models.autoSave();
        updateScrollPane();
        view.getStage().centerOnScreen();
        view.getStage().setTitle("Pinpoint | " + models.getCurrentTournament().getName());

        if (view.isDebugMode()) {
            addDebugButton();
        }
    }

    /**
     * Method for adding a debug button.
     */
    private void addDebugButton() {
        HBox parent = (HBox) searchBar.getParent();
        Button debugButton = new Button("Legg til test spill.");
        debugButton.setOnAction(actionEvent -> loadTestData());
        debugButton.setStyle("-fx-background-color: red");
        parent.getChildren().add(debugButton);

    }

    /**
     * Method for updating the information in the scroll pane
     */
    @FXML
    void updateScrollPane() {
        if (searchBar.getText().isBlank())
            updateGameCards(round.getAllGames());
        else {
            updateGameCards(round.getGamesByNameSearch(searchBar.getText().strip(), 30));
        }
        scrollPane.getChildren().clear();
        fillScrollPane();
    }

    /**
     * Method for updating the game cards in the game card list.
     * 
     * @param games
     *            to create from
     */
    private void updateGameCards(List<Game> games) {
        gameCards.clear();
        for (Game game : games) {
            gameCards.put(getGameCard(game), game);
        }
    }

    /**
     * Method for getting a score game card given the game
     * @param game game to make card from
     * @return card
     */
    private Node getGameCard(Game game) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 10px 12px 10px 12px;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: white;
                -fx-effect: dropshadow(three-pass-box , rgba(0,0,0,0.6) , 5, 0, 0, 1);
                """;
        int rows = 2;
        int columns = 6;

        HBox box = new HBox();
        Label player = new Label(game.getPlayer().toString());
        GridPane games = new GridPane();
        Region spacer = new Region();

        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                int gameNumber = column + (row / 2) * columns + 1;
                if (row % 2 == 0) {
                    TextField textField = new TextField();
                    if (game.getScores().size() > column) {
                        textField.setText(Integer.toString(game.getScores().get(column)));
                    }
                    textField.setId("points-" + game.getPlayer().getLicense() + "-" + gameNumber);
                    textField.setMaxWidth(60);
                    textField.setMinWidth(60);
                    textField.setStyle("""
                        -fx-background-color: -color-dark;
                        -fx-text-fill: white;
                        -fx-padding: 10px 10px 5px 10px;
                        -fx-border-insets: 3px 2px 0px 2px;
                        -fx-background-insets: 3px 2px 0px 2px;
                    """);
                    textField.setOnKeyTyped(keyEvent -> {
                        if (!textField.getText().isBlank()) {
                            try {
                                game.setScore(gameNumber - 1, Integer.parseInt(textField.getText()));
                            } catch (IllegalArgumentException e) {
                                Popups.showError("Feil", "Feil ved inntasting", e.getMessage());
                            }
                        } else {

                        }
                    });
                    games.add(textField, column, row);

                } else {
                    Label label = new Label("G" + gameNumber);
                    GridPane.setHalignment(label, Pos.CENTER.getHpos());
                    games.add(label, column, row);
                }
            }
        }


        box.getChildren().add(player);
        player.setMinWidth(400);
        HBox.setHgrow(spacer, Priority.ALWAYS);
        box.getChildren().add(spacer);
        box.getChildren().add(games);
        box.setStyle(cardStyle);
        box.setSpacing(20);
        return box;
    }

    /**
     * Method to fill the scroll plane with gameCards.
     */
    private void fillScrollPane() {
        for (Node card : gameCards.keySet()) {
            scrollPane.getChildren().add(card);
        }
    }

    /**
     * Method for loading test data.
     */
    @FXML
    private void loadTestData() {
        Random random = new Random();
        for (Game game : gameCards.values()) {
            if (game.getScores().size() == 0) {
                for (int i = 0; i < round.getPlaysPerPlayer(); i++) {
                    game.addScore(random.nextInt(300));
                }
            }
        }
        updateScrollPane();
    }

    @Override
    void onBackPress(ActionEvent event) {
        models.autoSave();
        view.setCurrentScene(View.OPENING_VIEW);
        view.getStage().setTitle("Pinpoint");
    }

    @FXML
    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_SCORE_SUMMARY);
        models.autoSave();
    }
}
