package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.Models;
import edu.ntnu.idatt1002.k2g05.View;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Base class for new open tournament controllers to inherit from. A Open tournamnet controller is a controller that
 * handles somthing after a tournament is opened
 */
abstract public class BaseOpenTournamentController {
    Models models = Models.getInstance();
    View view = View.getInstance();

    /**
     * When the back button is pressed.
     * 
     * @param event
     *            action event
     */
    @FXML
    abstract void onBackPress(ActionEvent event);

    /**
     * When the back button is pressed.
     * 
     * @param event
     *            action event
     */
    @FXML
    abstract void onNextPress(ActionEvent event);
}
