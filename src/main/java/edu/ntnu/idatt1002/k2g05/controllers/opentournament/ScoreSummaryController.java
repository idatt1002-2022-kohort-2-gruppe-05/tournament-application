package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class for the Score summary FXML file
 */
public class ScoreSummaryController extends BaseOpenTournamentController {

    private final QualificationRound qualificationRound = (QualificationRound) models.getCurrentTournament()
            .getRoundCollections(RoundType.QUALIFICATION_ROUND).orElseThrow().getAllRounds().get(0);
    private final RoundCollection groupPlayCollection = models.getCurrentTournament()
            .getRoundCollections(RoundType.GROUP_STAGE).orElseThrow();

    @FXML
    private VBox scrollPane;

    /**
     * Method that gets called after the FXML elements are loaded in.
     */
    public void initialize() {
        models.autoSave();
        if (!groupPlayIsCreated(groupPlayCollection)) {
            List<Player> topPlayers = qualificationRound.getTopGames(32).stream().map(Game::getPlayer).toList();
            RoundCollectionFactory.updateGroupStageCollectionWithSnakeFill(groupPlayCollection, topPlayers, 8);
        }
        updateScrollPane();
    }

    /**
     * Method for checking if a groupPlay is created or not
     * 
     * @param groupPlayCollection
     *            to check
     * @return true if created
     */
    private boolean groupPlayIsCreated(RoundCollection groupPlayCollection) {
        for (Round round : groupPlayCollection.getAllRounds()) {
            MatchRound matchRound = (MatchRound) round;
            if (matchRound.getAllMatchGames().isEmpty()) {
                return false;
            }
            for (MatchGame matchGame : matchRound.getAllMatchGames()) {
                if (matchGame.getAllMatches().isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Method for updating the scoll pane with the groupPlay data.
     */
    @FXML
    void updateScrollPane() {
        scrollPane.getChildren().clear();
        for (Round round : groupPlayCollection.getAllRounds()) {
            scrollPane.getChildren().add(getGroupCard((MatchRound) round));
        }
    }

    /**
     * Method for getting the group information on a card given the matchround
     * 
     * @param matchRound
     *            to create card from
     * @return the card.
     */
    private Node getGroupCard(MatchRound matchRound) {
        int[] spacing = new int[] { 20, 20, 20, 20, 20 };
        String[] columns = new String[] { "Spillere", "Land", "Klubb", "", "Total kvalifikasjons poeng" };

        List<Player> playersInMatchRound = matchRound.getAllMatchGames().stream().map(MatchGame::getPlayer).toList();
        List<Game> games = new ArrayList<>();
        for (Player player : playersInMatchRound) {
            games.add(qualificationRound.getGameByPlayer(player));
        }

        Label title = new Label(matchRound.getName());
        VBox mainContainer = new VBox(title, getHeaderCard(columns, spacing), getPlayersCard(spacing, games));

        mainContainer.setStyle("");
        mainContainer.setAlignment(Pos.BASELINE_LEFT);
        return mainContainer;
    }

    /**
     * Method for getting the header card
     * @param columns titles
     * @param spacing column in percent
     * @return the Node/card
     */
    private Node getHeaderCard(String[] columns, int[] spacing) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 6 20 6 20;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: -color-dark;
                """;
        GridPane header = new GridPane();
        header.getColumnConstraints().addAll(getColumnConstraints(spacing));
        for (int i = 0; i < columns.length; i++) {
            Label columnName = new Label(columns[i]);
            columnName.setStyle("-fx-text-fill: white;");
            header.addColumn(i, columnName);
        }
        header.setStyle(cardStyle);
        return header;
    }

    /**
     * Method for getting the player card given a list of games.
     * @param spacing of the columns
     * @param games to create card from
     * @return the Node/Card
     */
    private Node getPlayersCard(int[] spacing, List<Game> games) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 6 20 6 20;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: white;
                """;

        GridPane playersCard = new GridPane();
        playersCard.getColumnConstraints().addAll(getColumnConstraints(spacing));

        for (int i = 0; i < games.size(); i++) {
            Game game = games.get(i);
            Player player = game.getPlayer();



            Label name = new Label(player.getName());
            Label country = new Label(player.getCountry());
            Label club = new Label(player.getClub());
            Label space = new Label("");
            Label sum = new Label(Integer.toString(game.getSum()));


            playersCard.addRow(i, name, country, club, space, sum);
        }

        playersCard.setStyle(cardStyle);

        return playersCard;
    }

    /**
     * Method for getting the column constaints
     * 
     * @param spacing
     *            of the columns in percent
     * @return the list of column constraints
     */
    private List<ColumnConstraints> getColumnConstraints(int[] spacing) {
        List<ColumnConstraints> constraints = new ArrayList<>();
        for (int space : spacing) {
            ColumnConstraints con = new ColumnConstraints();
            con.setPercentWidth(25);
            constraints.add(con);
        }
        return constraints;
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_SCORE_REGISTRATION);
    }

    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_MATCH_REGISTRATION);
    }
}
