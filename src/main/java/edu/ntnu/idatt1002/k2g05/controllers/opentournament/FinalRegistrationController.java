package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.Group;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.MatchColumn;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.MatchPair;
import edu.ntnu.idatt1002.k2g05.controllers.opentournament.utils.PlayerPair;
import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.Tournament;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Controller for Final-Registration.
 */
public class FinalRegistrationController extends BaseOpenTournamentController {
    private final List<RoundCollection> finals;
    private final List<Boolean> finalsActivated;
    private final RoundCollection groupPlay;
    private final List<Group> groups = new ArrayList<>();

    @FXML
    private Accordion accordion;
    @FXML
    private HBox navigation;

    /**
     * Constructor method for early initialization.
     */
    public FinalRegistrationController() {
        finals = getFinals();
        finalsActivated = getActivatedFinals(finals);
        groupPlay = models.getCurrentTournament().getRoundCollections(RoundType.GROUP_STAGE).orElseThrow();
    }

    /**
     * Method that gets called after FXML elements are loaded
     */
    public void initialize() {
        models.autoSave();
        updateAccordion();

        if (view.isDebugMode()) {
            addDebugButton();
        }
    }

    private void addDebugButton() {
        Button debugButton = new Button("Legg til test spill.");
        debugButton.setOnAction(actionEvent -> loadTestData());
        debugButton.setStyle("-fx-background-color: red");
        navigation.getChildren().add(debugButton);
    }

    /**
     * Methof for checking if a final is done, and it is possible to move to the next final. If yes it will unlock the
     * next stage.
     */
    private void checkIfCanProceed() {
        for (int i = 0; i < finals.size(); i++) {

            boolean done = false;
            if (finalsActivated.get(i)) {
                for (MatchColumn matchColumn : groups.get(i).getMatchColumns()) {
                    for (Node node : matchColumn.getAllNodes()) {
                        Set<Node> textFields = node.lookupAll("TextField");
                        for (Node textField : textFields) {
                            if (!((TextField) textField).getText().isBlank()) {
                                done = true;
                            }
                        }
                    }
                }
            }

            if (done && i + 1 < finals.size()) {
                if (!finalIsLoaded(finals.get(i + 1))) {
                    finalsActivated.set(i + 1, true);
                    RoundCollectionFactory.fillFinalFromPreviousFinal(finals.get(i + 1), finals.get(i));
                    updateAccordion();
                }
            }

        }
    }

    /**
     * Method for checking if some finals already contains data, if no it wil only activate the first final.
     * 
     * @param finals
     *            to check
     * @return list of boolean relating to which finals are activated.
     */
    private List<Boolean> getActivatedFinals(List<RoundCollection> finals) {
        List<Boolean> activated = new ArrayList<>();
        for (RoundCollection aFinal : finals) {
            activated.add(finalIsLoaded(aFinal));
        }

        // If none is activated, activate first.
        if (activated.stream().noneMatch(o -> o)) {
            activated.set(0, true);
        }

        return activated;
    }

    /**
     * Method for getting all the finals.
     * 
     * @return list of finals.
     */
    private List<RoundCollection> getFinals() {
        List<RoundCollection> finals = new ArrayList<>();
        Tournament currentTournament = models.getCurrentTournament();

        finals.add(currentTournament.getRoundCollections(RoundType.QUARTER_FINAL).orElseThrow());
        finals.add(currentTournament.getRoundCollections(RoundType.SEMI_FINAL).orElseThrow());
        finals.add(currentTournament.getRoundCollections(RoundType.FINAL).orElseThrow());

        return finals;
    }

    /**
     * Method for updating the accordion, with data in groups.
     */
    public void updateAccordion() {
        updateFinals();
        accordion.getPanes().clear();
        for (Group group : groups) {
            accordion.getPanes().add(getGroupPane(group));
        }
    }

    /**
     * Method for updating the finals
     */
    private void updateFinals() {
        if (!finalIsLoaded(finals.get(0))) {
            try {
                RoundCollectionFactory.fillFinalFromGroupPlay(finals.get(0), groupPlay);
            } catch (IllegalArgumentException e) {
                Popups.showError("Feil", "Noe Galt skjedde", e.getMessage());
            }
        }

        groups.clear();
        for (RoundCollection finalCollection : finals) {
            Group group = new Group(finalCollection.getName());

            int columns = finalCollection.getAmountOfRounds();

            for (int col = 0; col < columns; col++) {
                MatchColumn matchColumn = new MatchColumn(col);
                for (MatchGame matchGame : ((MatchRound) finalCollection.getRound(col)).getAllMatchGames()) {
                    Match match = matchGame.getAllMatches().get(0);
                    PlayerPair pair = getPlayerPair(matchGame, match);

                    if (matchColumn.containsPlayerPair(pair)) {
                        matchColumn.createMatchPair(pair, match);
                    } else {
                        matchColumn.addPlayerPairAndMatch(pair, match);
                    }
                }

                for (PlayerPair pair : matchColumn.getAllPlayerPairs()) {
                    MatchPair matchPair = matchColumn.getMatchPairByPlayerPair(pair);
                    matchColumn.addPlayerPairAndNode(pair, getPlayerPairNode(pair, matchPair));
                }

                group.addMatchColumn(matchColumn);
            }

            groups.add(group);
        }
    }

    /**
     * Method for checking if a final is loaded or not. loaded means That it has been filled with players and matches
     * @param aFinal to check if it is loaded
     * @return true if it is loaded.
     */
    private boolean finalIsLoaded(RoundCollection aFinal) {
        boolean done = true;
        for (Round round : aFinal.getAllRounds()) {
            if (!(round instanceof MatchRound matchRound)) {
                throw new IllegalArgumentException("Final must contain matchrounds");
            }
            done = !matchRound.getAllMatchGames().stream().map(MatchGame::getPlayed).toList().isEmpty();
        }
        return done;
    }

    /**
     * Method for getting a Node from a Player pair and a correlating matchPair.
     * 
     * @param pair
     *            to create from
     * @param matchPair
     *            to create
     * @return the Node
     */
    private Node getPlayerPairNode(PlayerPair pair, MatchPair matchPair) {
        HBox matchPane = new HBox();

        HBox player1Pane = new HBox();
        HBox player2Pane = new HBox();

        Player player1 = pair.p1();
        Player player2 = pair.p2();

        Match match1 = matchPair.getMatchByPlayer(player1);
        Match match2 = matchPair.getMatchByPlayer(player2);

        Label name1 = new Label(player1.getInitials());
        name1.setTooltip(new Tooltip(player1.getName()));

        Label name2 = new Label(player2.getInitials());
        name2.setTooltip(new Tooltip(player2.getName()));

        TextField score1 = new TextField();
        score1.setOnKeyTyped(a -> {
            if (!score1.getText().isBlank()) {
                try {
                    match1.setPlayerScore(Integer.parseInt(score1.getText()));
                    match2.setOpponentScore(Integer.parseInt(score1.getText()));
                } catch (NumberFormatException e) {
                    Popups.showError("Feil", "Noe Gikk galt:", e.getMessage());
                    score1.clear();
                }
            }
        });
        TextField score2 = new TextField();
        score2.setOnKeyTyped(a -> {
            if (!score2.getText().isBlank()) {
                try {
                    match1.setOpponentScore(Integer.parseInt(score2.getText()));
                    match2.setPlayerScore(Integer.parseInt(score2.getText()));
                } catch (NumberFormatException e) {
                    Popups.showError("Feil", "Noe Gikk galt:", e.getMessage());
                    score2.clear();
                }
            }
        });

        if (match1.getPlayerScore() != -1) {
            score1.setText(Integer.toString(match1.getPlayerScore()));
        }

        if (match2.getPlayerScore() != -1) {
            score2.setText(Integer.toString(match2.getPlayerScore()));
        }

        score1.setPrefWidth(80);
        score2.setPrefWidth(80);

        player1Pane.getChildren().addAll(name1, score1);
        player2Pane.getChildren().addAll(score2, name2);

        player1Pane.setPrefWidth(150);
        player2Pane.setPrefWidth(150);

        matchPane.getChildren().addAll(player1Pane, player2Pane);

        return matchPane;
    }

    /**
     * Method for creating a PlayerPair given matchGame and a match
     * 
     * @param matchGame
     *            to be created from
     * @param match
     *            to be created from
     * @return the created player pair.
     */
    private PlayerPair getPlayerPair(MatchGame matchGame, Match match) {
        return new PlayerPair(matchGame.getPlayer(), match.getOpponent());
    }

    /**
     * Method for getting a pane of info from a group
     * 
     * @param group
     *            to be created from
     * @return the title pane
     */
    private TitledPane getGroupPane(Group group) {
        GridPane gridPane = new GridPane();

        for (MatchColumn matchColumn : group.getMatchColumns()) {
            Label label = new Label("Match " + (matchColumn.getIndex() + 1));
            label.setAlignment(Pos.CENTER);
            gridPane.add(label, matchColumn.getIndex(), 0);
            int row = 1;
            for (Node node : matchColumn.getAllNodes()) {
                gridPane.add(node, matchColumn.getIndex(), row);
                row++;
            }
        }

        // Button should not appear if it's a final
        if (group.getMatchColumns().size() != 1) {
            Button tryNext = new Button("Gå videre.");
            tryNext.setAlignment(Pos.CENTER);
            tryNext.setOnAction(actionEvent -> checkIfCanProceed());
            GridPane.setHalignment(tryNext, HPos.CENTER);
            gridPane.add(tryNext, 0, group.getMatchColumns().get(0).getAllNodes().size() + 1,
                    group.getMatchColumns().size(), 1);
        }

        ScrollPane scrollPane = new ScrollPane(gridPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        TitledPane titledPane = new TitledPane();
        titledPane.setText(group.getName());
        titledPane.setContent(scrollPane);

        if (group.getMatchColumns().stream().map(MatchColumn::getAllPlayerPairs).toList().get(0).isEmpty()) {
            titledPane.setDisable(true);
        }

        return titledPane;
    }

    /**
     * Method for loading test data.
     */
    @FXML
    private void loadTestData() {
        Random random = new Random();
        Set<Node> textFields = accordion.lookupAll("TextField");
        for (Node node : textFields) {
            ((TextField) node).setText(Integer.toString(random.nextInt(300)));
            node.fireEvent(new KeyEvent(node, node, KeyEvent.KEY_TYPED, "", "", KeyCode.UNDEFINED, false, false, false,
                    false));
        }
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_MATCH_SUMMARY);
    }

    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_FINALS_SUMMARY);
    }
}
