package edu.ntnu.idatt1002.k2g05.controllers.newtournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.factories.RoundsTemplateFactory;
import edu.ntnu.idatt1002.k2g05.models.Tournament;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundsTemplate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;

public class FormatController extends BaseNewTournamentController {

    @FXML
    RadioButton WMChoice;

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_1);
    }

    @Override
    void onNextPress(ActionEvent event) {
        loadTemplateInTournament();

        view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_3);
    }

    /**
     * Method for fetching the tournament template and loading it into the tournament.
     */
    private void loadTemplateInTournament() {
        RoundsTemplate template = getChosenTemplate();
        models.getCurrentTournament().setRoundsTemplate(template);
        Tournament tournament = models.getCurrentTournament();
        tournament.clearRoundCollections();

        for (RoundType roundType : template.getRounds()) {
            switch (roundType) {
            case QUALIFICATION_ROUND -> tournament
                    .addRoundCollections(RoundCollectionFactory.getEmptyQualificationCollection("Kvalifikasjon"));
            case GROUP_STAGE -> tournament
                    .addRoundCollections(RoundCollectionFactory.getEmptyGroupStageCollection("Gruppe spill", 4));
            case QUARTER_FINAL -> tournament
                    .addRoundCollections(RoundCollectionFactory.getEmptyQuarterFinalCollection("Kvart-Finale"));
            case SEMI_FINAL -> tournament
                    .addRoundCollections(RoundCollectionFactory.getEmptySemiFinalCollection("Semi-Finale"));
            case FINAL -> tournament.addRoundCollections(RoundCollectionFactory.getEmptyFinalCollection("Finale"));
            }
        }
    }

    /**
     * Method when the standard format picture is pressed.
     */
    @FXML
    void onStandardFormatClick() {
        Popups.showError("Støtte", "Standard format er ikke støttet enda.", "");
    }

    /**
     * Method when the standard format picture is pressed.
     */
    @FXML
    void onBracketsFormatClick() {
        Popups.showError("Støtte", "Brackets format er ikke støttet enda.", "");
    }

    /**
     * Gets the chosen template by looking at what button has been pressed.
     * 
     * @return the chosen template
     */
    private RoundsTemplate getChosenTemplate() {
        if (WMChoice.isSelected()) {
            return RoundsTemplateFactory.getWorldChampionshipsTemplate();
        } else {
            return null;
        }
    }
}
