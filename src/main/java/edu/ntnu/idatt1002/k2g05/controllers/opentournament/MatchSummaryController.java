package edu.ntnu.idatt1002.k2g05.controllers.opentournament;

import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for th match summary FXML.
 */
public class MatchSummaryController extends BaseOpenTournamentController {

    private final RoundCollection groupPlay = models.getCurrentTournament().getRoundCollections(RoundType.GROUP_STAGE)
            .orElseThrow();

    private SortType currentSortType = SortType.POINTS;

    @FXML
    private VBox scrollPane;

    /**
     * Method that gets called after the FXML has loaded
     */
    public void initialize() {
        models.autoSave();
        updateScrollPane();
    }

    /**
     * Method for updateing the scroll pane with new data
     */
    @FXML
    void updateScrollPane() {
        scrollPane.getChildren().clear();
        for (Round round : groupPlay.getAllRounds()) {
            scrollPane.getChildren().add(getGroupCard((MatchRound) round));
        }
    }

    /**
     * Method for getting a group card given the match round.
     * 
     * @param matchRound
     *            to create from
     * @return Node/Card
     */
    private Node getGroupCard(MatchRound matchRound) {
        int[] spacing = new int[] { 25, 22, 10, 10, 10, 10, 13 };
        String[] columns = new String[] { "Spiller", "Land", "Spilt", "Vunnet", "Uavgjordt", "Tapt", "Poeng" };

        Label title = new Label(matchRound.getName());
        matchRound.sort(currentSortType, true);
        VBox mainContainer = new VBox(title, getHeaderCard(columns, spacing),
                getPlayersCard(spacing, matchRound.getAllMatchGames()));

        mainContainer.setStyle("");
        mainContainer.setAlignment(Pos.BASELINE_LEFT);
        return mainContainer;
    }

    /**
     * Method for getting the header cards. Which holds the title for coulm
     * @param columns titles
     * @param spacing of columns by percent
     * @return the Node/Card
     */
    private Node getHeaderCard(String[] columns, int[] spacing) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 6 20 6 20;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: -color-dark;
                """;
        GridPane header = new GridPane();
        header.getColumnConstraints().addAll(getColumnConstraints(spacing));
        for (int i = 0; i < columns.length; i++) {
            Label columnName = new Label(columns[i]);
            columnName.setStyle("-fx-text-fill: white;");
            header.addColumn(i, columnName);
        }
        header.setStyle(cardStyle);
        return header;
    }

    /**
     * Method for getting the player node/card with information
     * @param spacing of the columns in percent
     * @param matchGames to show data from
     * @return the node/card
     */
    private Node getPlayersCard(int[] spacing, List<MatchGame> matchGames) {
        final String cardStyle = """
                -fx-outline: none;
                -fx-line-height: 1;
                -fx-background-radius: 10px;
                -fx-padding: 6 20 6 20;
                -fx-border-insets: 5px;
                -fx-background-insets: 5px;
                -fx-background-color: white;
                """;

        GridPane playersCard = new GridPane();
        playersCard.getColumnConstraints().addAll(getColumnConstraints(spacing));

        for (int i = 0; i < matchGames.size(); i++) {
            MatchGame matchGame = matchGames.get(i);
            Player player = matchGame.getPlayer();

            //  { "Spiller", "Land", "Spilt", "Vunnet", "Uavgjordt", "Tapt", "Poeng" };
            Label name = new Label(player.getName());
            Label country = new Label(player.getCountry());
            Label played = new Label(Integer.toString(matchGame.getPlayed()));
            Label won = new Label(Integer.toString(matchGame.getWon()));
            Label draws = new Label(Integer.toString(matchGame.getDraw()));
            Label lost = new Label(Integer.toString(matchGame.getLost()));
            Label points = new Label(Integer.toString(matchGame.getPoints()));


            playersCard.addRow(i, name, country, played, won, draws, lost, points);
        }

        playersCard.setStyle(cardStyle);

        return playersCard;
    }

    /**
     * Method for getting a list of column contains given an array of percentage widths
     * 
     * @param spacing
     *            of the constainst
     * @return list of constraints
     */
    private List<ColumnConstraints> getColumnConstraints(int[] spacing) {
        List<ColumnConstraints> constraints = new ArrayList<>();
        for (int space : spacing) {
            ColumnConstraints con = new ColumnConstraints();
            con.setPercentWidth(space);
            constraints.add(con);
        }
        return constraints;
    }

    @Override
    void onBackPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_MATCH_REGISTRATION);
    }

    @Override
    void onNextPress(ActionEvent event) {
        view.setCurrentScene(View.OPEN_TOURNAMENT_FINALS_REGISTRATION);
    }

}
