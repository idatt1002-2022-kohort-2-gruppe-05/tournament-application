package edu.ntnu.idatt1002.k2g05.controllers;

import edu.ntnu.idatt1002.k2g05.Models;
import edu.ntnu.idatt1002.k2g05.View;
import edu.ntnu.idatt1002.k2g05.models.League;
import edu.ntnu.idatt1002.k2g05.models.Tournament;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

/**
 * Controller for the Opening-View
 */
public class OpeningController {
    private final View view = View.getInstance();
    private final Models models = Models.getInstance();

    @FXML
    private VBox recentlyOpened;

    /**
     * Method that gets called after FXML elements are loaded.
     */
    public void initialize() {
        view.getStage().centerOnScreen();
    }

    /**
     * Method that gets called when the new Tournament button is pressed
     * 
     * @param event
     *            event
     */
    @FXML
    private void newTournamentPress(ActionEvent event) {
        view.setCurrentScene(View.NEW_TOURNAMENT_VIEW_1);
        view.getStage().centerOnScreen();
    }

    /**
     * Method that gets called when the new Tournament button is pressed
     * 
     * @param event
     *            event
     */
    @FXML
    private void openTournamentPress(ActionEvent event) {
        try {
            FileChooser fileChooser = new FileChooser();

            // Set extension filter for text files
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Tournament Files (*.trnm)",
                    "*.trnm");
            fileChooser.getExtensionFilters().add(extFilter);

            // Show save file dialog
            File file = fileChooser.showOpenDialog(view.getStage());

            if (file != null) {
                models.setCurrentTournament(Tournament.load(file.getAbsolutePath()));
                view.setCurrentScene(View.OPEN_TOURNAMENT_SCORE_REGISTRATION);
                view.getStage().centerOnScreen();
            }
        } catch (Exception e) {
            Popups.showError("Feil", "Noe Gikk Galt:", e.getMessage());
        }
    }
}
