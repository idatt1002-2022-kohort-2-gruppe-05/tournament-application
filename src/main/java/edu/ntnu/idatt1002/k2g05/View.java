package edu.ntnu.idatt1002.k2g05;

import edu.ntnu.idatt1002.k2g05.models.Tournament;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Singleton class for managing the current view.
 */
public class View {
    private static View instance;

    private Stage stage;
    private Scene currentScene;

    private boolean debugMode = false;

    public final static String OPENING_VIEW = "views/opening-view.fxml";
    public final static String NEW_TOURNAMENT_VIEW_1 = "views/new-tournament/view.fxml";
    public final static String NEW_TOURNAMENT_VIEW_2 = "views/new-tournament/format-view.fxml";
    public final static String NEW_TOURNAMENT_VIEW_3 = "views/new-tournament/add-player-view.fxml";
    public final static String NEW_TOURNAMENT_VIEW_4 = "views/new-tournament/rounds.fxml";
    public final static String OPEN_TOURNAMENT_SCORE_REGISTRATION = "views/open-tournament/score-registration.fxml";
    public final static String OPEN_TOURNAMENT_SCORE_SUMMARY = "views/open-tournament/score-summary.fxml";
    public final static String OPEN_TOURNAMENT_MATCH_REGISTRATION = "views/open-tournament/match-registration.fxml";
    public final static String OPEN_TOURNAMENT_MATCH_SUMMARY = "views/open-tournament/match-summary.fxml";
    public final static String OPEN_TOURNAMENT_FINALS_REGISTRATION = "views/open-tournament/finals-registration.fxml";
    public final static String OPEN_TOURNAMENT_FINALS_SUMMARY = "views/open-tournament/final-summary-view.fxml";

    private View() {
    }

    /**
     * Method for getting the instance of the View class.
     * 
     * @return the instance
     */
    public static synchronized View getInstance() {
        if (instance == null) {
            instance = new View();
        }
        return instance;
    }

    /**
     * Gets the stage, the stage is the window frame, and can be seen as the parent of the view.
     * 
     * @return the stages
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Sets the stage, the stage is the window frame, and can be seen as the parent of the view.
     * 
     * @param stage
     *            to be set.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Method for getting the currently set scene, the scene is the content of the window.
     * 
     * @return the scene
     */
    public Scene getCurrentScene() {
        return currentScene;
    }

    /**
     * Method for setting the current scene, the scene is the content of the window.
     * 
     * @param currentScene
     *            to be set
     */
    public void setCurrentScene(Scene currentScene) {
        this.currentScene = currentScene;
        updateStage();
    }

    /**
     * Sets the current scene, it is preferred to use the static constants in this class to set a scene, as they will be
     * correct.
     *
     * @param path
     *            to FXML file
     * @throws IllegalArgumentException
     *             if the path is wrong or the file is wrong.
     */
    public void setCurrentScene(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(View.class.getResource(path));
        try {
            this.currentScene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Something went wrong with loading the FXML file:\n" + e.getMessage());
        }
        updateStage();
    }

    /**
     * Method for updating the stage with the current scene.
     */
    private void updateStage() {
        this.stage.setScene(this.currentScene);
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean b) {
        debugMode = b;
    }
}
