package edu.ntnu.idatt1002.k2g05;

/**
 * Launcher class for packaging purposes.
 */
public class Launcher {
    public static void main(String[] args) {
        App.main(args);
    }
}
