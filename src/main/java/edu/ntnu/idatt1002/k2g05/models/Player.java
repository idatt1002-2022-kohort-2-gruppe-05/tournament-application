package edu.ntnu.idatt1002.k2g05.models;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class for holding a player in a competiton.
 */
public class Player implements Serializable {
    @Serial
    private static final long serialVersionUID = 6682871319901889170L;
    private final String name;
    private final char gender;
    private final char playerClass;
    private final String club;
    private final int license;
    private final double handicap;
    private final String country;

    /**
     * Constructor for creating an instance of a player given his/her information.
     *
     * @param name
     *            of the player, full name. Can not be blank
     * @param gender
     *            of the player, must be 'M' for male of 'F' for female
     * @param playerClass
     *            class of the player
     * @param club
     *            that the player is associated with
     * @param license
     *            of the player
     * @param handicap
     *            of the player, must be positive.
     * @param country
     *            that the player is from
     *
     * @throws IllegalArgumentException
     *             if arguments are not ut to par.
     * @throws NullPointerException
     *             if arguments are null.
     */
    public Player(String name, char gender, char playerClass, String club, int license, double handicap,
            String country) {
        Objects.requireNonNull(name);
        if (name.isBlank()) {
            throw new IllegalArgumentException("Navn kan ikke være tomt");
        }
        if (gender != 'F' && gender != 'M') {
            throw new IllegalArgumentException("Kjønn må være F eller M.");
        }
        if (!Character.isAlphabetic(playerClass) || Character.isLowerCase(playerClass)) {
            throw new IllegalArgumentException("Klasse må være storbokstav og en bokstav.");
        }
        Objects.requireNonNull(club);
        if (club.isBlank()) {
            throw new IllegalArgumentException("Klubb kan ikke være blank");
        }
        if (license < 0) {
            throw new IllegalArgumentException("Lisens må være over 0");
        }
        if (handicap < 0) {
            throw new IllegalArgumentException("Handicap må kan ikke være negativ (0+)");
        }
        Objects.requireNonNull(country);
        if (country.isBlank()) {
            throw new IllegalArgumentException("Land kan ikke være tomt");
        }
        this.name = name;
        this.gender = gender;
        this.playerClass = playerClass;
        this.club = club;
        this.license = license;
        this.handicap = handicap;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public char getPlayerClass() {
        return playerClass;
    }

    public String getClub() {
        return club;
    }

    public int getLicense() {
        return license;
    }

    public double getHandicap() {
        return handicap;
    }

    public String getCountry() {
        return country;
    }

    /**
     * Gets the initials of the player by spitting the name and getting every first character of the name.
     * 
     * @return the initials.
     */
    public String getInitials() {
        return Arrays.stream(name.split(" ")).map(s -> s.substring(0, 1)).collect(Collectors.joining());
    }

    @Override
    public String toString() {
        return name;
    }
}
