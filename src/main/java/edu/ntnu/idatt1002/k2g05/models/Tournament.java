package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.rounds.*;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

/**
 * Class for holding a tournament and all its data.
 */
public class Tournament implements Serializable {
    @Serial
    private static final long serialVersionUID = 1839265436589560768L;
    private String name;
    private final List<RoundCollection> roundCollections;
    private final HashSet<Player> players;
    private League league;
    private LocalDate date;
    private String city;
    private String bowlingAlley;

    private RoundsTemplate roundsTemplate;
    private String lastSaveLocation;

    /**
     * Constructor of the Tournament class for creating an instance of the class.
     * 
     * @param name
     *            of the tournament
     * @param league
     *            for the tournament
     * @param date
     *            for the start of the tournament
     * @param city
     *            where the tournament is located.
     * @param bowlingAlly
     *            where the tournament is played.
     */
    public Tournament(String name, League league, LocalDate date, String city, String bowlingAlly) {
        setName(name);
        setLeague(league);
        setDate(date);
        setCity(city);
        setBowlingAlley(bowlingAlly);

        this.players = new HashSet<>();
        this.roundCollections = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    /**
     * Sets the name of the tournament.
     * 
     * @param name
     *            to be set.
     */
    public void setName(String name) {
        Objects.requireNonNull(name);
        if (name.isBlank()) {
            throw new IllegalArgumentException("Navn kan ikke være blankt");
        }
        this.name = name;
    }

    /**
     * Gets all the RoundCollections in the tournament
     * 
     * @return the roundCollections
     */
    public List<RoundCollection> getAllRoundCollections() {
        return this.roundCollections;
    }

    /**
     * Get a specific roundCollection given its insertion order (index) in the tournament.
     * 
     * @param index
     *            index of the roundCollection to return
     * @return the roundCollection at the specified position in the tournament
     * @throws IndexOutOfBoundsException
     *             if the index is out of range ({@code index < 0 || index >= size()})
     */
    public RoundCollection getRoundCollections(int index) {
        return this.roundCollections.get(index);
    }

    /**
     * Returns an {@link Optional} describing the first roundCollection match of the given type, or an empty
     * {@code Optional} if it does not contain any roundCollection with this type.
     *
     * @param type
     *            of the collection to get.
     * @return an {@code Optional} describing the first roundCollection match of the given type, or an empty
     *         {@code Optional} if it does not contain any roundCollection with this type.
     * @throws NullPointerException
     *             if the tournament does not contain any roundcollections
     */
    public Optional<RoundCollection> getRoundCollections(RoundType type) {
        return this.roundCollections.stream().filter(roundCollection -> roundCollection.getType() == type).findFirst();
    }

    /**
     * Appends the specified roundCollection to the end of the tournament's roundCollections list. {@code null} is
     * prohibited.
     *
     * @param roundCollection
     *            element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws NullPointerException
     *             if the specified element is null
     */
    public boolean addRoundCollections(RoundCollection roundCollection) {
        return this.roundCollections.add(Objects.requireNonNull(roundCollection, "Roundcollection. can not be zero"));
    }

    /**
     * Removes the first occurrence of a roundCollection from this list, if it is present. If this tournament does not
     * contain the roundCollection, it is unchanged. Returns {@code true} if this list contained the specified element
     * (or equivalently, if this list changed as a result of the call).
     *
     * @param roundCollection
     *            roundCollection to be removed from this tournament, if present
     * @return {@code true} if this list contained the specified round.
     */
    public boolean removeRoundCollection(RoundCollection roundCollection) {
        return roundCollections.remove(roundCollection);
    }

    /**
     * Removes the roundCollection at the specified position in this list (optional operation). Shifts any subsequent
     * elements to the left (subtracts one from their indices). Returns the element that was removed from the list.
     *
     * @param index
     *            the index of the roundCollection to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException
     *             if the index is out of range ({@code index < 0 || index >= size()})
     */
    public RoundCollection removeRoundCollection(int index) {
        return roundCollections.remove(index);
    }

    /**
     * Gets all the players in the tournament.
     * 
     * @return all the players
     */
    public HashSet<Player> getAllPlayers() {
        return players;
    }

    /**
     * Method for returning a list over results given a search query by name. A fuzzy search algorithm is used so the
     * user does not have to write the exact player name.
     * 
     * @param search
     *            to be filtered by
     * @param threshold
     *            of the fuzzy search, the higher the value the more specific a search has to be. Threshold must be
     *            between 0 and 100
     * @return
     */
    public List<Player> getPlayersBySearch(String search, int threshold) {
        if (threshold < 0 || threshold > 100) {
            throw new IllegalArgumentException("Threshold must be between 0 and 100");
        }

        return this.players
                .stream().filter(o -> FuzzySearch.ratio(search, o.getName()) > threshold).sorted((o1, o2) -> Integer
                        .compare(FuzzySearch.ratio(search, o2.getName()), FuzzySearch.ratio(search, o1.getName())))
                .toList();

    }

    /**
     * Method for adding players to the tournament.
     * 
     * @param player
     *            to be added ot the list
     * @throws IllegalArgumentException
     *             if the player is already added to the tournament.
     */
    public void addPlayer(Player player) {
        if (!players.add(player)) {
            throw new IllegalArgumentException("Spiller er allerede i turneringen.");
        }
    }

    /**
     * Method for removing a given player from the torunament
     * 
     * @param player
     *            to be removed.
     * @return {@code true} if the tournament contained the specified element
     * @throws NullPointerException
     *             if the given player is null
     */
    public boolean removePlayer(Player player) {
        return players.remove(Objects.requireNonNull(player));
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        Objects.requireNonNull(league, "Du må velge enten Herre, Dame eller Junior");
        this.league = league;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        Objects.requireNonNull(date, "Dato må være valgt.");
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBowlingAlley() {
        return bowlingAlley;
    }

    public void setBowlingAlley(String bowlingAlley) {
        this.bowlingAlley = bowlingAlley;
    }

    public int getAmountOfPlayers() {
        return this.players.size();
    }

    public RoundsTemplate getRoundsTemplate() {
        return roundsTemplate;
    }

    public void setRoundsTemplate(RoundsTemplate roundsTemplate) {
        this.roundsTemplate = roundsTemplate;
    }

    /**
     * Method for saving the current instance of the tournament to a serialized file.
     * 
     * @param url
     *            to save to
     * @return {@code true} if the save was successful
     * @throws UncheckedIOException
     * @throws IllegalArgumentException
     *             if url is blank or it doesn't end in .trnm
     */
    public boolean save(String url) {
        if (url.isBlank()) {
            throw new IllegalArgumentException("Du må velge filnavn for å lagre filen.");
        } else if (!url.endsWith(".trnm")) {
            throw new IllegalArgumentException("Filnavn må slutte med '.trnm' (Turnerings fil)");
        }
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(url))) {
            objectOutputStream.writeObject(this);
            lastSaveLocation = url;
            return true;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String getLastSaveLocation() {
        return lastSaveLocation;
    }

    public void setLastSaveLocation(String lastSaveLocation) {
        this.lastSaveLocation = lastSaveLocation;
    }

    /**
     * Method for getting an instance of a tournament given a serialized file.
     * 
     * @param url
     *            of the file
     * @return the tournament
     * @throws ClassNotFoundException
     *             Class of a serialized object cannot be found.
     */
    public static Tournament load(String url) throws ClassNotFoundException {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(url))) {
            Tournament tournament = (Tournament) objectInputStream.readObject();
            tournament.setLastSaveLocation(url);
            return tournament;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(
                name + " | " + league + " | " + date.toString() + " | " + city + " - " + bowlingAlley);
        this.roundCollections.forEach(o -> str.append("\n\t-").append(o.toString()));
        return str.toString();
    }

    public void clearRoundCollections() {
        roundCollections.clear();
    }
}
