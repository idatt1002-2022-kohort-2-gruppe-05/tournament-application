package edu.ntnu.idatt1002.k2g05.models.games;

import edu.ntnu.idatt1002.k2g05.models.Player;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * A match game holds a player and all of his matches against other people. This will create duplicates, e.g if player
 * "A" plays a match against player "B", "B" has also played a match against player "A". The reason for storing the data
 * this way is that it makes it a lot easier to fin the amount of points a player has earned. But in hindsight, it would
 * probably be better to not store duplicates because the front-end application is going to have to manage the
 * duplicates.
 */
public class MatchGame implements Serializable {
    private static final int WON_POINTS = 3;
    private static final int DRAW_POINTS = 1;
    private static final int LOST_POINTS = 0;
    @Serial
    private static final long serialVersionUID = 2587153988349625826L;

    private final List<Match> matches;
    private final Player player;

    /**
     * Constructor for creating an instance of the game, given the player that plays it.
     * 
     * @param player
     */
    public MatchGame(Player player) {
        this.player = player;
        matches = new ArrayList<>();
    }

    /**
     * Gets the points that a player has gotten accross its matches. The amount of points rewarded for winning, drawing
     * and losing is determined by the constants of the class.
     *
     * @return the points
     */
    public int getPoints() {
        return getWon() * WON_POINTS + getDraw() * DRAW_POINTS + getLost() * LOST_POINTS;
    }

    /**
     * Method for getting the amount matches that is a win
     * 
     * @return number of wins
     */
    public int getWon() {
        return matches.stream().mapToInt(o -> o.getMatchResult() == 1 ? 1 : 0).sum();
    }

    /**
     * Method for getting the amount matches that is lost
     *
     * @return number lost
     */
    public int getLost() {
        return matches.stream().mapToInt(o -> o.getMatchResult() == -1 ? 1 : 0).sum();
    }

    /**
     * Method for getting the amount matches that is a draw
     *
     * @return number of draws
     */
    public int getDraw() {
        return matches.stream().mapToInt(o -> o.getMatchResult() == 0 ? 1 : 0).sum();
    }

    public int getPlayed() {
        return matches.size();
    }

    /**
     * Appends the specified match to the end of the matchGame's list. {@code null} is prohibited.
     *
     * @param match
     *            element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws NullPointerException
     *             if the specified match is null
     */
    public boolean addMatch(Match match) {
        Objects.requireNonNull(match);
        return matches.add(match);
    }

    /**
     * Removes the first occurrence of a match from this matchGame, if it is present. If this matchGame does not contain
     * the match, it is unchanged. Returns {@code true} if this list contained the specified match (or equivalently, if
     * this list changed as a result of the call).
     *
     * @param match
     *            match to be removed from this tournament, if present
     * @return {@code true} if this list contained the specified match.
     */
    public boolean removeMatch(Match match) {
        Objects.requireNonNull(match);
        return matches.remove(match);
    }

    public List<Match> getAllMatches() {
        return matches;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return "MatchGame" + "\n\t-Player: " + player;
    }

    /**
     * Method for clearing all the matches played.
     */
    public void clear() {
        matches.clear();
    }
}