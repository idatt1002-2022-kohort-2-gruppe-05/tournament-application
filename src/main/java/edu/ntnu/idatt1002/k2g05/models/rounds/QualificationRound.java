package edu.ntnu.idatt1002.k2g05.models.rounds;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import me.xdrop.fuzzywuzzy.FuzzySearch;

import java.io.Serial;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Round where each player plays an amount given amount of {@link Game}, and an accumulative sum of all rounds are
 * created.
 */
public class QualificationRound extends Round {

    @Serial
    private static final long serialVersionUID = -5925450141994077751L;
    private final List<Game> games;
    private final int playsPerPlayer;

    /**
     * Constructor for creating an instance of the given class. Plays per player will be defaulted to 6.
     * 
     * @param name
     *            of the round, can not be null
     */
    public QualificationRound(String name) {
        super(name);
        games = new ArrayList<>();
        playsPerPlayer = 6;
    }

    /**
     * Constructor for creating an instance of the class.
     * 
     * @param name
     *            of the round.
     * @param playsPerPlayer
     *            amount of games a player is going to play.
     */
    public QualificationRound(String name, int playsPerPlayer) {
        super(name);
        games = new ArrayList<>();
        this.playsPerPlayer = playsPerPlayer;
    }

    /**
     * Method for adding game to the round.
     *
     * @param game
     *            to be added ot the list
     *
     * @throws NullPointerException
     *             if the specified game is null
     * @return {@code true} (as specified by {@link Collection#add})
     */
    public boolean addGame(Game game) {
        Objects.requireNonNull(game);
        return games.add(game);
    }

    /**
     * Removes the first occurrence of a game from this round, if it is present. If this round does not contain the
     * game, it is unchanged. Returns {@code true} if this list contained the specified game (or equivalently, if this
     * round changed as a result of the call).
     *
     *
     * @param game
     *            to be removed
     * @return {@code true} if this round contained the specified game
     */
    public boolean removeGame(Game game) {
        Objects.requireNonNull(game);
        return games.remove(game);
    }

    /**
     * Method for getting all the games in the round
     * 
     * @return
     */
    public List<Game> getAllGames() {
        return games;
    }

    /**
     * Method for returning a list over results given a search query by name. A fuzzy search algorithm is used so the
     * user does not have to write the exact player name.
     *
     * @param search
     *            to be filtered by
     * @param threshold
     *            of the fuzzy search, the higher the value the more specific a search has to be. Threshold must be
     *            between 0 and 100
     * @return the results of the search.
     */
    public List<Game> getGamesByNameSearch(String search, int threshold) {
        return this.games.stream().filter(o -> FuzzySearch.ratio(search, o.getPlayer().getName()) > threshold)
                .sorted((o1, o2) -> Integer.compare(FuzzySearch.ratio(search, o2.getPlayer().getName()),
                        FuzzySearch.ratio(search, o1.getPlayer().getName())))
                .toList();
    }

    /**
     * Method for getting a Game given the player that has played it.
     * 
     * @param player
     * @return
     */
    public Game getGameByPlayer(Player player) {
        return games.stream().filter(game -> game.getPlayer() == player).findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    /**
     * Method for getting the top n games in the round, "top" is defined by the sum that the game has If the n-th player
     * and the n+1-th player has the same point score it will be up to the previous sorted arrangement of the games.
     * 
     * @param n
     *            amount of top players to get
     * @return n top players
     */
    public List<Game> getTopGames(int n) {
        sort(SortType.PLACEMENT, true);
        return games.subList(0, n);
    }

    public int getPlaysPerPlayer() {
        return playsPerPlayer;
    }

    /**
     * Method for sorting the games in the round given a sort type
     * 
     * @param sortType
     *            to sort by.
     */
    public void sort(SortType sortType) {
        switch (sortType) {
        case PLAYER -> games.sort((Comparator.comparing(o -> o.getPlayer().getName())));
        case COUNTRY -> games.sort((Comparator.comparing(o -> o.getPlayer().getCountry())));
        case PLACEMENT -> games.sort((Comparator.comparing(Game::getSum)));
        }
    }

    /**
     * Method for sorting the games in the round given a sort type, and if it should be reversed.
     * 
     * @param sortType
     *            to be sorted by.
     * @param reversed
     *            true if yes,
     */
    public void sort(SortType sortType, boolean reversed) {
        sort(sortType);
        if (reversed) {
            Collections.reverse(games);
        }
    }

    @Override
    public void clear() {
        games.clear();
    }

}
