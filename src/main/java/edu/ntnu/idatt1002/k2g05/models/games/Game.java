package edu.ntnu.idatt1002.k2g05.models.games;

import edu.ntnu.idatt1002.k2g05.models.Player;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that holds a game, a game is composed of several scores, a score is the sum of all the throws in a play.
 */
public class Game implements Serializable {
    @Serial
    private static final long serialVersionUID = -9093758850497793012L;
    private final Player player;
    private final List<Integer> scores;

    /**
     * Constructor for creating an instance of a Game.
     * 
     * @param player
     *            the player who played
     */
    public Game(Player player) {
        this.player = player;
        this.scores = new ArrayList<>();
    }

    /**
     * Constructor for creating an instance of a Game.
     * 
     * @param player
     *            the player who played
     * @param scores
     *            list of scores the player has played.
     */
    public Game(Player player, List<Integer> scores) {
        this.player = player;
        this.scores = scores;
    }

    /**
     * Gets the player that has played the game.
     * 
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method for getting all the scores that a player has played.
     * 
     * @return all the scores.
     */
    public List<Integer> getScores() {
        return scores;
    }

    /**
     * Method for adding a score to the game
     * 
     * @param score
     *            to be added. must be between 0 and 300
     * @return true if this collection changed as a result of the call
     */
    public boolean addScore(int score) {
        if (score < 0 || score > 300) {
            throw new IllegalArgumentException("Resultat må være mellom 0 og 300.");
        }
        return scores.add(score);
    }

    /**
     * Removes the score at the specified position in this list. Shifts any subsequent elements to the left (subtracts
     * one from their indices). Returns the element that was removed from the list. Params:
     * 
     * @param index
     *            – the index of the element to be removed
     * @return the score previously at the specified position Throws: IndexOutOfBoundsException – if the index is out of
     *         range
     */
    public int removeScore(int index) {
        return scores.remove(index);
    }

    /**
     * Returns the sum of all scores.
     * 
     * @return sum
     */
    public int getSum() {
        return scores.stream().mapToInt(Integer::intValue).sum();
    }

    /**
     * Returns the arithmetic average of all the scores.
     * 
     * @return the mean
     */
    public double getMean() {
        if (scores.size() == 0) {
            return 0;
        }
        return (double) scores.stream().mapToInt(Integer::intValue).sum() / scores.size();
    }

    /**
     * Method for setting a score at a index, if index is out of bounds it will add scores to fit to index.
     * 
     * @param index
     *            to be added to
     * @param score
     *            to add.
     */
    public void setScore(int index, int score) {
        while (scores.size() - 1 < index) {
            scores.add(0);
        }
        scores.set(index, score);
    }
}
