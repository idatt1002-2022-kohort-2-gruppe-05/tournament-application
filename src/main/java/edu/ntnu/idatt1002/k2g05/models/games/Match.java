package edu.ntnu.idatt1002.k2g05.models.games;

import edu.ntnu.idatt1002.k2g05.models.Player;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;

/**
 * Class that holds a Match. A match is defined as a match played against an opponent and the scores of the opponent and
 * the player that holds the match
 */

public class Match implements Serializable {
    @Serial
    private static final long serialVersionUID = -8213191668183407766L;
    private final Player opponent;
    private int opponentScore;
    private int playerScore;

    /**
     * Constructor for creating an instance for a match between two players
     * 
     * @param opponent
     *            the Player opponent
     * @param opponentScore
     *            the opponent's score as an int
     * @param playerScore
     *            the Player's score as an int
     */
    public Match(Player opponent, int opponentScore, int playerScore) {
        this.opponent = opponent;
        setOpponentScore(opponentScore);
        setPlayerScore(playerScore);
    }

    /**
     * Sets scores to be -1 which means they have not been played yet
     * 
     * @param opponent
     */
    public Match(Player opponent) {
        this.opponent = opponent;
        opponentScore = -1;
        playerScore = -1;
    }

    /**
     * Method to determine whether the player has won, drew, or lost the game
     * 
     * @return 1 if the player won, 0 if draw between player and opponent occurs, -1 if player loses
     */
    public int getMatchResult() {
        return Integer.compare(playerScore, opponentScore);
    }

    public Player getOpponent() {
        return opponent;
    }

    public int getOpponentScore() {
        return opponentScore;
    }

    /**
     * Sets the score of the opponent
     * 
     * @param opponentScore
     *            score of the opponent, can not be less than zero
     * @throws IllegalArgumentException
     *             if score is less than zero.
     */
    public void setOpponentScore(int opponentScore) {
        if (opponentScore < 0) {
            throw new IllegalArgumentException("The opponent's score cannot contain negative values");
        }
        this.opponentScore = opponentScore;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    /**
     * Sets the score of the player that holds the match
     * 
     * @param playerScore
     *            score of the opponent, can not be less than zero
     * @throws IllegalArgumentException
     *             if score is less than zero.
     */
    public void setPlayerScore(int playerScore) {
        if (playerScore < 0) {
            throw new IllegalArgumentException("The player's score cannot contain negative values");
        }
        this.playerScore = playerScore;
    }
}
