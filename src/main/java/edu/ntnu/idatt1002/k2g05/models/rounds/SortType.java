package edu.ntnu.idatt1002.k2g05.models.rounds;

/**
 * Types to sort by.
 */
public enum SortType {
    PLAYER, COUNTRY, WON, DRAW, LOST, POINTS, PLAYED, PLACEMENT
}
