package edu.ntnu.idatt1002.k2g05.models;

/**
 * Leauge of a tournament can be JUNIOR, MALE or FEMALE.
 */
public enum League {
    JUNIOR, MALE, FEMALE
}
