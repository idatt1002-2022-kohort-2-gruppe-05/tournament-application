package edu.ntnu.idatt1002.k2g05.models.rounds;

/**
 * Different types of rounds.
 */
public enum RoundType {
    QUALIFICATION_ROUND, GROUP_STAGE, SIXTEENTH_FINALS, QUARTER_FINAL, SEMI_FINAL, FINAL,
}
