package edu.ntnu.idatt1002.k2g05.models.rounds;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Class for holding a template of a round. A template is a description of how a tournament is going to be by rounds
 * type.
 */
public class RoundsTemplate implements Serializable {
    @Serial
    private static final long serialVersionUID = 5706941374419609292L;
    List<RoundType> rounds;

    /**
     * Constructor for creating a new template.
     */
    public RoundsTemplate() {
        this.rounds = new ArrayList<>();
    }

    /**
     * Appends the specified roundType to the end of the template. {@code null} is prohibited.
     *
     * @param roundType
     *            to be appended to this template
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws NullPointerException
     *             if the specified element is null
     */
    public boolean addRound(RoundType roundType) {
        return rounds.add(Objects.requireNonNull(roundType));
    }

    /**
     * Removes the first occurrence of a roundType from the template, if it is present. If the template does not contain
     * the roundType, it is unchanged. Returns {@code true} if this list contained the specified element (or
     * equivalently, if this list changed as a result of the call).
     *
     * @param roundType
     *            to be removed from this tournament, if present
     * @return {@code true} if this list contained the specified roundType.
     */
    public boolean removeRound(RoundType roundType) {
        return rounds.remove(roundType);
    }

    public List<RoundType> getRounds() {
        return rounds;
    }
}
