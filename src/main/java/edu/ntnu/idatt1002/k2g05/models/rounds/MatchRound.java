package edu.ntnu.idatt1002.k2g05.models.rounds;

import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;

import java.io.Serial;
import java.util.*;

/**
 * A match round holds multiple match games, some examples of a match round would be a group-play or a semi-final, where
 * the matches that is in match round is a list of {@link MatchGame}.
 */
public class MatchRound extends Round {
    @Serial
    private static final long serialVersionUID = 3465257445442233837L;
    private final List<MatchGame> matchGames;
    private final boolean isFinal;

    /**
     * Constructor for making an instance of a MatchRound. Given the name. isFinal will be set to false.
     *
     * @param name
     *            of the round
     */
    public MatchRound(String name) {
        super(name);
        isFinal = false;
        this.matchGames = new ArrayList<>();
    }

    /**
     * Constructor for making an instance of a MatchRound. Given the name and if it is a final
     *
     * @param name
     *            of the round
     * @param isFinal
     *            true if yes.
     */
    public MatchRound(String name, boolean isFinal) {
        super(name);
        this.matchGames = new ArrayList<>();
        this.isFinal = isFinal;
    }

    @Override
    public void clear() {
        matchGames.clear();
    }

    /**
     * Method for adding matchGame to the round.
     *
     * @param matchGame
     *            to be added ot the list
     *
     * @throws NullPointerException
     *             if the specified matchGame is null
     * @return {@code true} (as specified by {@link Collection#add})
     */
    public boolean addMatchGame(MatchGame matchGame) {
        Objects.requireNonNull(matchGame);
        return matchGames.add(matchGame);
    }

    /**
     * Method for getting the top n matchGames in the round, "top" is defined by the points that the matchGames has If
     * the n-th player and the n+1-th player has the same point score it will be up to the previous sorted arrangement
     * of the games.
     * 
     * @param n
     *            amount of top players to get
     * @return n top players
     */
    public List<MatchGame> getTopMatchGames(int n) {
        sort(SortType.PLACEMENT, true);
        return matchGames.subList(0, n);
    }

    /**
     * Removes the first occurrence of a matchGame from this round, if it is present. If this round does not contain the
     * matchGame, it is unchanged. Returns {@code true} if this list contained the specified matchGame (or equivalently,
     * if this round changed as a result of the call).
     *
     *
     * @param matchGame
     *            to be removed
     * @return {@code true} if this round contained the specified matchGame
     */
    public boolean removeMatchGame(MatchGame matchGame) {
        Objects.requireNonNull(matchGame);
        return matchGames.remove(matchGame);
    }

    /**
     * Method for sorting the matchGames in the round given a sort type
     * 
     * @param sortType
     *            to sort by.
     */
    public void sort(SortType sortType) {
        switch (sortType) {
        case PLAYER -> matchGames.sort(Comparator.comparing(o -> o.getPlayer().getName()));
        case COUNTRY -> matchGames.sort(Comparator.comparing(o -> o.getPlayer().getCountry()));
        case PLAYED -> matchGames.sort(Comparator.comparing(MatchGame::getPlayed));
        case WON -> matchGames.sort(Comparator.comparing(MatchGame::getWon));
        case DRAW -> matchGames.sort(Comparator.comparing(MatchGame::getDraw));
        case LOST -> matchGames.sort(Comparator.comparing(MatchGame::getLost));
        case POINTS, PLACEMENT -> matchGames.sort(Comparator.comparing(MatchGame::getPoints));
        }
    }

    /**
     * Method for getting the amount of match games in the round
     * 
     * @return the amount of match games.
     */
    public int getAmountMatchGames() {
        return matchGames.size();
    }

    /**
     * Method for getting all the matchGames in the round.
     * 
     * @return all the match games
     */
    public List<MatchGame> getAllMatchGames() {
        return matchGames;
    }

    /**
     * Get a specific matchGame given its insertion order (index) in the round.
     *
     * @param index
     *            index of the matchGame to return
     * @return the matchGame at the specified position in the round
     * @throws IndexOutOfBoundsException
     *             if the index is out of range ({@code index < 0 || index >= size()})
     */
    public MatchGame getMatchGame(int index) {
        return matchGames.get(index);
    }

    public boolean isFinal() {
        return isFinal;
    }

    /**
     * Method for sorting the matchGames in the round given a sort type, and if it should be reversed.
     * 
     * @param sortType
     *            to be sorted by.
     * @param reversed
     *            true if yes,
     */
    public void sort(SortType sortType, boolean reversed) {
        sort(sortType);
        if (reversed) {
            Collections.reverse(matchGames);
        }
    }
}
