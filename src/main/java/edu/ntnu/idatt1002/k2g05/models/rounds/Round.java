package edu.ntnu.idatt1002.k2g05.models.rounds;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * A class that holds a round, a round is a collection of multiple games, a round could be f.eks "Semi-final" , "Final"
 */
abstract public class Round implements Serializable {
    @Serial
    private static final long serialVersionUID = 2412487239032796410L;
    private String name;

    /**
     * Constructor for making an instance of a round.
     * 
     * @param name
     *            of the round, can not be blank.
     */
    public Round(String name) {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Navnet på runden kan ikke være tomt");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Method for setting the name of the Round, name can not be blank or {@code null}
     * 
     * @param name
     *            to be set
     * @throws IllegalArgumentException
     *             if name is blank
     * @throws NullPointerException
     *             if name is null
     */
    public void setName(String name) {
        Objects.requireNonNull(name);
        if (name.isBlank()) {
            throw new IllegalArgumentException("Navn kan ikke være null");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Method for clearing the round of games.
     */
    public abstract void clear();

}
