package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Class for holding multiple rounds. An example of a roundCollection could be "group rounds", which would hold multiple
 * {@link MatchRound}
 */
public class RoundCollection implements Serializable {
    @Serial
    private static final long serialVersionUID = -8997430778022684293L;
    private final List<Round> rounds;
    String name;
    private RoundType type;

    /**
     * Constructor for creating an instance of a round collection.
     * 
     * @param name
     *            of the round collection
     */
    public RoundCollection(String name) {
        this.name = name;
        this.rounds = new ArrayList<>();
    }

    /**
     * Constructor for creating an instance of a round collection. Given a name and the type of the rounds in the
     * collection.
     * 
     * @param name
     *            of the round collection
     * @param type
     *            of the round collection
     */
    public RoundCollection(String name, RoundType type) {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Navnet på rundesamlingen kan ikke være tomt");
        }
        this.name = name;
        this.rounds = new ArrayList<>();
        this.type = type;
    }

    /**
     * Gets all the rounds in the round collection.
     * 
     * @return
     */
    public List<Round> getAllRounds() {
        return rounds;
    }

    public Round getRound(int index) {
        return rounds.get(index);
    }

    /**
     * Gets the amount of rounds in the collection
     * 
     * @return amount of rounds in the collection.
     */
    public int getAmountOfRounds() {
        return rounds.size();
    }

    /**
     * Appends the specified round to the end of the roundCollection. {@code null} is prohibited.
     *
     * @param round
     *            element to be appended to this collection
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws NullPointerException
     *             if the specified element is null
     */
    public boolean addRound(Round round) {
        if (Objects.isNull(round)) {
            throw new NullPointerException("runden som blir sent inn er ikke initialisiert");
        }
        return this.rounds.add(round);
    }

    /**
     * Removes the first occurrence of a round from this collection, if it is present. If this roundCollection does not
     * contain the round, it is unchanged. Returns {@code true} if the collection contained the specified round (or
     * equivalently, if this list changed as a result of the call).
     *
     * @param round
     *            round to be removed from this roundCollection, if present
     * @return {@code true} if this list contained the specified round.
     */
    public boolean removeRound(Round round) {
        if (Objects.isNull(round)) {
            throw new NullPointerException("runden som blir fjernet er ikke initialisiert");
        }
        return this.rounds.remove(round);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(name + ": " + rounds.size() + " rounds:");
        for (Round round : rounds) {
            str.append("\n\t-").append(round.toString());
        }
        return str.toString();
    }

    public RoundType getType() {
        return type;
    }

    public void setType(RoundType type) {
        this.type = type;
    }
}
