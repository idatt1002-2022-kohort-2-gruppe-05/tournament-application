package edu.ntnu.idatt1002.k2g05;

import edu.ntnu.idatt1002.k2g05.controllers.Popups;
import edu.ntnu.idatt1002.k2g05.models.Tournament;

/**
 * Singleton class which holds data for the application. To get the instance of the object use the
 * <code>getInstance()</code> method.
 */
public class Models {
    private static Models instance;

    private Tournament currentTournament;

    private Models() {
    }

    /**
     * Method for returning the single instance of the class, which contains data.
     * 
     * @return the instance
     */
    public static synchronized Models getInstance() {
        if (instance == null) {
            instance = new Models();
        }
        return instance;
    }

    /**
     * Gets the current set tournament.
     * 
     * @return the tournament.
     */
    public Tournament getCurrentTournament() {
        return currentTournament;
    }

    /**
     * Method for saving the file to its previous chosen path @return true if save was successful. @throws
     *
     */
    public boolean autoSave() {
        if (currentTournament.getLastSaveLocation() == null) {
            throw new NullPointerException("Tournament has no previous saved location");
        }

        return currentTournament.save(currentTournament.getLastSaveLocation());
    }

    /**
     * Sets the current tournament.
     * 
     * @param tournament
     *            to be set.
     */
    public void setCurrentTournament(Tournament tournament) {
        this.currentTournament = tournament;
    }
}
