package edu.ntnu.idatt1002.k2g05.debug;

import com.github.javafaker.Faker;
import edu.ntnu.idatt1002.k2g05.models.Player;
import javafx.fxml.FXML;

import java.util.Random;

public class DebugData {
    // DEBUG purposes
    private static final Faker faker = new Faker(); // Random name generator to get unique names for each iteration
    private static final Random random = new Random();

    /**
     * Method to get an element from a random array of chars
     * 
     * @param array
     *            An array
     * @return A random element from the array
     */
    private static char getRandomChar(char[] array) {
        int rnd = random.nextInt(array.length);
        return array[rnd];
    }

    /**
     * Method to get a random licence number for a player
     * 
     * @return A five-digit ints
     */
    private static int getRandomLicence() {
        int min = 10000;
        int max = 99999;
        return random.nextInt(max - min) + min;
    }

    /**
     * Method for calling a new test player to be used in test classes
     * 
     * @return a new player
     */
    public static Player getTestPlayer() {
        return new Player(faker.name().name(), getRandomChar(new char[] { 'M', 'F' }),
                getRandomChar(new char[] { 'A', 'B', 'C', 'D', 'E', 'F' }), faker.company().name(), getRandomLicence(),
                random.nextDouble(100), faker.country().name());
    }

}
