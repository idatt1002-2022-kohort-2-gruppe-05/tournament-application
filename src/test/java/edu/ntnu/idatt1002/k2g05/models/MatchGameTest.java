package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MatchGameTest {

    @Test
    void testGetPoints() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        assertEquals(7, testMatchGame.getPoints());
    }

    @Test
    void testGetWon() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        assertEquals(2, testMatchGame.getWon());
    }

    @Test
    void testGetLost() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        assertEquals(2, testMatchGame.getLost());
    }

    @Test
    void testGetDraw() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        assertEquals(1, testMatchGame.getDraw());
    }

    @Test
    void testGetPlayed() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        assertEquals(5, testMatchGame.getPlayed());
    }

    @Test
    void testGetAllMatches() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);

        List<Match> testList = new ArrayList<>();
        testList.add(testMatch);
        testList.add(testMatch2);
        testList.add(testMatch3);
        testList.add(testMatch4);
        testList.add(testMatch5);

        assertEquals(testList, testMatchGame.getAllMatches());
        assertEquals(5, testMatchGame.getPlayed());
    }

    @Test
    void testRemoveMatch() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch4 = new Match(TestData.getTestPlayer(), 10, 5);
        Match testMatch5 = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);
        testMatchGame.addMatch(testMatch2);
        testMatchGame.addMatch(testMatch3);
        testMatchGame.addMatch(testMatch4);
        testMatchGame.addMatch(testMatch5);
        testMatchGame.removeMatch(testMatch);

        List<Match> testList = new ArrayList<>();
        testList.add(testMatch2);
        testList.add(testMatch3);
        testList.add(testMatch4);
        testList.add(testMatch5);

        assertEquals(4, testMatchGame.getPlayed());
        assertEquals(testList, testMatchGame.getAllMatches());
    }

    @Test
    void testThatAddMatchWithNullThrowsException() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        assertThrows(NullPointerException.class, () -> testMatchGame.addMatch(null));
    }

    @Test
    void testThatRemoveMatchWithNullThrowsException() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        Match testMatch = new Match(TestData.getTestPlayer(), 10, 20);

        testMatchGame.addMatch(testMatch);

        assertThrows(NullPointerException.class, () -> testMatchGame.removeMatch(null));
    }
}