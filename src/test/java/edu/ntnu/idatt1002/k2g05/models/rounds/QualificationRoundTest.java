package edu.ntnu.idatt1002.k2g05.models.rounds;

import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.TestData;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class QualificationRoundTest {

    @Test
    void testGetTopGames() {
        QualificationRound testQRound = new QualificationRound("testName");
        for (int i = 0; i < 50; i++) {
            testQRound.addGame(TestData.getTestGameWithScores(6));

        }

        int topAmount = 32;
        List<Game> topGames = testQRound.getTopGames(topAmount);

        assertEquals(topAmount, topGames.size());

    }

    @Test
    void testAddGame() {
        QualificationRound testQRound = new QualificationRound("testName");
        Game testGame = new Game(TestData.getTestPlayer());
        testQRound.addGame(testGame);

        assertTrue(testQRound.getAllGames().contains(testGame));
    }

    @Test
    void testRemoveGame() {
        QualificationRound testQRound = new QualificationRound("testName");
        Game testGame = new Game(TestData.getTestPlayer());
        testQRound.addGame(testGame);
        testQRound.removeGame(testGame);

        assertFalse(testQRound.getAllGames().contains(testGame));
    }

    @Test
    void testGetAllGames() {
        QualificationRound testQRound = new QualificationRound("testName");

        Game testGame1 = new Game(TestData.getTestPlayer());
        Game testGame2 = new Game(TestData.getTestPlayer());
        Game testGame3 = new Game(TestData.getTestPlayer());
        testQRound.addGame(testGame1);
        testQRound.addGame(testGame2);
        testQRound.addGame(testGame3);

        Game[] correctList = new Game[] { testGame1, testGame2, testGame3 };

        assertArrayEquals(correctList, testQRound.getAllGames().toArray());
    }

    @Test
    void testSort() {
        QualificationRound testQRound = new QualificationRound("testName");

        Player testPlayer1 = new Player("aTestName1", 'M', 'S', "testClub1", 1234, 0, "ctestCountry1");
        Player testPlayer2 = new Player("bTestName2", 'F', 'A', "testClub2", 4321, 1, "btestCountry2");
        Player testPlayer3 = new Player("cTestName3", 'M', 'A', "testClub2", 5678, 1, "atestCountry3");

        Game testGame1 = new Game(testPlayer1);
        Game testGame2 = new Game(testPlayer2);
        Game testGame3 = new Game(testPlayer3);

        testGame1.addScore(2);
        testGame2.addScore(5);
        testGame3.addScore(10);

        testQRound.addGame(testGame3);
        testQRound.addGame(testGame2);
        testQRound.addGame(testGame1);

        Game[] correctPlayerSort = new Game[] { testGame1, testGame2, testGame3 };
        testQRound.sort(SortType.PLAYER);
        assertArrayEquals(correctPlayerSort, testQRound.getAllGames().toArray());

        Game[] correctCountrySort = new Game[] { testGame3, testGame2, testGame1 };
        testQRound.sort(SortType.COUNTRY);
        assertArrayEquals(correctCountrySort, testQRound.getAllGames().toArray());

        Game[] correctPlacementSort = new Game[] { testGame1, testGame2, testGame3 };
        testQRound.sort(SortType.PLACEMENT);
        assertArrayEquals(correctPlacementSort, testQRound.getAllGames().toArray());

        Game[] correctPlacementReversedSort = new Game[] { testGame3, testGame2, testGame1 };
        testQRound.sort(SortType.PLACEMENT, true);
        assertArrayEquals(correctPlacementReversedSort, testQRound.getAllGames().toArray());
    }

    @Test
    void testThatAddGameWithNullThrowsException() {
        QualificationRound testQRound = new QualificationRound("testName");

        assertThrows(NullPointerException.class, () -> testQRound.addGame(null));
    }

    @Test
    void testThatRemoveGameWithNullThrowsException() {
        QualificationRound testQRound = new QualificationRound("testName");

        assertThrows(NullPointerException.class, () -> testQRound.removeGame(null));
    }
}