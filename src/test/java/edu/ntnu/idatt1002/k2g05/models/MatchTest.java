package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.games.Match;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatchTest {

    @Test
    void testGetMatchResult() {
        Match testMatch1 = new Match(TestData.getTestPlayer(), 10, 15);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 10, 10);
        Match testMatch3 = new Match(TestData.getTestPlayer(), 20, 15);

        assertEquals(1, testMatch1.getMatchResult());
        assertEquals(0, testMatch2.getMatchResult());
        assertEquals(-1, testMatch3.getMatchResult());
    }

    @Test
    void testIfNegativeValuesCanBeAssignedToPlayerScore() {
        assertThrows(IllegalArgumentException.class, () -> new Match(TestData.getTestPlayer(), -10, -15));
    }

    @Test
    void testGetOpponentScore() {
        Match testMatch1 = new Match(TestData.getTestPlayer(), 10, 15);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 12, 20);

        assertEquals(10, testMatch1.getOpponentScore());
        assertEquals(12, testMatch2.getOpponentScore());
    }

    @Test
    void testGetPlayerScore() {
        Match testMatch1 = new Match(TestData.getTestPlayer(), 10, 15);
        Match testMatch2 = new Match(TestData.getTestPlayer(), 12, 20);

        assertEquals(15, testMatch1.getPlayerScore());
        assertEquals(20, testMatch2.getPlayerScore());
    }
}