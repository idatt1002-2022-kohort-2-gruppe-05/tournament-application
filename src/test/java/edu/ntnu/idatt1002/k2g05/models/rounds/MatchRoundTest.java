package edu.ntnu.idatt1002.k2g05.models.rounds;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.TestData;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MatchRoundTest {

    @Test
    void testAddMatchGame() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 20));

        MatchGame testMatchGame2 = new MatchGame(TestData.getTestPlayer());

        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 20));

        MatchRound testMatchRound = new MatchRound("testName");

        testMatchRound.addMatchGame(testMatchGame);
        testMatchRound.addMatchGame(testMatchGame2);

        List<MatchGame> testList = new ArrayList<>();
        testList.add(testMatchGame);
        testList.add(testMatchGame2);

        assertEquals(2, testMatchRound.getAmountMatchGames());
        assertEquals(testList, testMatchRound.getAllMatchGames());

    }

    @Test
    void testRemoveMatchGame() {
        MatchGame testMatchGame = new MatchGame(TestData.getTestPlayer());

        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame.addMatch(new Match(TestData.getTestPlayer(), 10, 20));

        MatchGame testMatchGame2 = new MatchGame(TestData.getTestPlayer());

        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 20));

        MatchRound testMatchRound = new MatchRound("testName");

        testMatchRound.addMatchGame(testMatchGame);
        testMatchRound.addMatchGame(testMatchGame2);
        testMatchRound.removeMatchGame(testMatchGame);

        List<MatchGame> testList = new ArrayList<>();
        testList.add(testMatchGame2);

        assertEquals(1, testMatchRound.getAmountMatchGames());
        assertEquals(testList, testMatchRound.getAllMatchGames());
    }

    @Test
    void testSort() {
        Player testPlayer1 = new Player("aTestName1", 'M', 'S', "testClub1", 1234, 0, "ctestCountry1");
        Player testPlayer2 = new Player("bTestName2", 'F', 'A', "testClub2", 4321, 1, "btestCountry2");
        Player testPlayer3 = new Player("cTestName3", 'M', 'A', "testClub2", 5678, 1, "atestCountry3");

        MatchGame testMatchGame1 = new MatchGame(testPlayer1);
        MatchGame testMatchGame2 = new MatchGame(testPlayer2);
        MatchGame testMatchGame3 = new MatchGame(testPlayer3);

        MatchRound testMatchRound = new MatchRound("testName");

        testMatchGame1.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame1.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame1.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame1.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame1.addMatch(new Match(TestData.getTestPlayer(), 10, 20));

        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 20));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame2.addMatch(new Match(TestData.getTestPlayer(), 10, 5));

        testMatchGame3.addMatch(new Match(TestData.getTestPlayer(), 10, 10));
        testMatchGame3.addMatch(new Match(TestData.getTestPlayer(), 10, 5));
        testMatchGame3.addMatch(new Match(TestData.getTestPlayer(), 10, 10));

        testMatchRound.addMatchGame(testMatchGame3);
        testMatchRound.addMatchGame(testMatchGame2);
        testMatchRound.addMatchGame(testMatchGame1);

        MatchGame[] correctPlayerSort = new MatchGame[] { testMatchGame1, testMatchGame2, testMatchGame3 };
        testMatchRound.sort(SortType.PLAYER);
        assertArrayEquals(correctPlayerSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctCountrySort = new MatchGame[] { testMatchGame3, testMatchGame2, testMatchGame1 };
        testMatchRound.sort(SortType.COUNTRY);
        assertArrayEquals(correctCountrySort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctPlayedSort = new MatchGame[] { testMatchGame3, testMatchGame2, testMatchGame1 };
        testMatchRound.sort(SortType.PLAYED);
        assertArrayEquals(correctPlayedSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctWonSort = new MatchGame[] { testMatchGame3, testMatchGame2, testMatchGame1 };
        testMatchRound.sort(SortType.WON);
        assertArrayEquals(correctWonSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctDrawSort = new MatchGame[] { testMatchGame2, testMatchGame1, testMatchGame3 };
        testMatchRound.sort(SortType.DRAW);
        assertArrayEquals(correctDrawSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctLostSort = new MatchGame[] { testMatchGame3, testMatchGame2, testMatchGame1 };
        testMatchRound.sort(SortType.LOST);
        assertArrayEquals(correctLostSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctPointsSort = new MatchGame[] { testMatchGame3, testMatchGame2, testMatchGame1 };
        testMatchRound.sort(SortType.POINTS);
        assertArrayEquals(correctPointsSort, testMatchRound.getAllMatchGames().toArray());

        MatchGame[] correctPointsReversedSort = new MatchGame[] { testMatchGame1, testMatchGame2, testMatchGame3 };
        testMatchRound.sort(SortType.POINTS, true);
        assertArrayEquals(correctPointsReversedSort, testMatchRound.getAllMatchGames().toArray());
    }

    @Test
    void testThatAddMatchGameWithNullThrowsException() {
        MatchRound testMatchRound = new MatchRound("testName");

        assertThrows(NullPointerException.class, () -> testMatchRound.addMatchGame(null));
    }

    @Test
    void testThatRemoveMatchGameWithNullThrowsException() {
        MatchRound testMatchRound = new MatchRound("testName");

        assertThrows(NullPointerException.class, () -> testMatchRound.removeMatchGame(null));
    }
}
