package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.games.Game;
import edu.ntnu.idatt1002.k2g05.models.games.Match;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

public class TournamentTest {

    @Test
    void testIfNoNameCanBeAssignedToANewTournament() {
        assertThrows(IllegalArgumentException.class, () -> TestData.getTestTournament().setName(""));
    }

    @Test
    void testIfTwoIdenticalPlayersCanBeAddedToTheSameTournament() {
        Player testPlayer = TestData.getTestPlayer();
        Tournament testTournament = TestData.getTestTournament();

        testTournament.addPlayer(testPlayer);

        assertThrows(IllegalArgumentException.class, () -> testTournament.addPlayer(testPlayer));
    }

}
