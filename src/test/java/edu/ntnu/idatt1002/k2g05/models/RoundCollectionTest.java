package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.factories.RoundCollectionFactory;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RoundCollectionTest {

    @Test
    void testGetAllRounds() {
        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 4);
        MatchRound matchRound = new MatchRound("test matchround", false);

        collection.addRound(matchRound);
        assertEquals(collection.getAllRounds().size(), 5);
        assertEquals(matchRound, collection.getAllRounds().get(4));
    }

    @Test
    void testGetAmountOfRounds() {
        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 10);

        assertEquals(collection.getAmountOfRounds(), 10);
        Round round = collection.getAllRounds().get(0);
        Round roundTwo = collection.getAllRounds().get(1);

        collection.removeRound(round);
        collection.removeRound(roundTwo);

        assertEquals(collection.getAmountOfRounds(), 8);
    }

    @Test
    void testAddRound() {
        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 4);
        MatchRound matchRound = new MatchRound("test matchround", false);

        collection.addRound(matchRound);
        assertEquals(collection.getAllRounds().size(), 5);
        assertEquals(matchRound, collection.getAllRounds().get(4));

    }

    @Test
    void testIfAddRoundThrowsExeptionIfRondIsNull() {

        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 4);
        MatchRound matchRound = new MatchRound("test matchround", false);

        Round round = null;

        try {
            collection.addRound(round);
            fail();
        } catch (NullPointerException e) {
            assertEquals(e.getMessage(), "runden som blir sent inn er ikke initialisiert");
        }

        assertThrows(NullPointerException.class, () -> {
            collection.addRound(round);
        });

    }

    @Test
    void testRemoveRound() {
        RoundCollection collection = RoundCollectionFactory.getEmptyQuarterFinalCollection("test quarterfinal");

        assertEquals(collection.getAmountOfRounds(), 4);

        Round roundOne = collection.getAllRounds().get(0);
        Round roundTwo = collection.getAllRounds().get(1);

        collection.removeRound(roundOne);
        collection.removeRound(roundTwo);

        assertEquals(collection.getAmountOfRounds(), 2);
    }

    @Test
    void testIfRemoveRoundThrowsExeptionIfRondIsNull() {

        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 4);
        MatchRound matchRound = new MatchRound("test matchround", false);

        Round round = null;

        try {
            collection.removeRound(round);
            fail();
        } catch (NullPointerException e) {
            assertEquals(e.getMessage(), "runden som blir fjernet er ikke initialisiert");
        }

        assertThrows(NullPointerException.class, () -> {
            collection.addRound(round);
        });

    }
}