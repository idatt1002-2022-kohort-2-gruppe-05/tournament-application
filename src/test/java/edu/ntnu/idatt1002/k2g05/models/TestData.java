package edu.ntnu.idatt1002.k2g05.models;

import com.github.javafaker.Faker;
import edu.ntnu.idatt1002.k2g05.models.games.Game;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestData {

    private static Faker faker = new Faker(); // Random name generator to get unique names for each iteration

    private static Random random = new Random();

    /**
     * Method to get an element from a random array of chars
     * 
     * @param array
     *            An array
     * @return A random element from the array
     */
    private static char getRandomChar(char[] array) {
        int rnd = random.nextInt(array.length);
        return array[rnd];
    }

    /**
     * Method to get a random licence number for a player
     * 
     * @return A five-digit int
     */
    private static int getRandomLicence() {
        int min = 10000;
        int max = 99999;
        return random.nextInt(max - min) + min;
    }

    /**
     * Method to retrieve a random League-type from the League class
     * 
     * @return a random League-type as an int
     */
    public static League getRandomLeague() {
        int pick = random.nextInt(League.values().length);
        return League.values()[pick];
    }

    /**
     * Method for calling a new test tournament to be used in test classes
     * 
     * @return a new tournament
     */
    public static Tournament getTestTournament() {
        return new Tournament(faker.lordOfTheRings().location() + "Supreme Master's Cup", getRandomLeague(),
                LocalDate.now(), faker.country().capital(), faker.country().capital() + " Bowling Ally");
    }

    /**
     * Method for calling a new test player to be used in test classes
     * 
     * @return a new player
     */
    public static Player getTestPlayer() {
        return new Player(faker.name().fullName(), getRandomChar(new char[] { 'M', 'F' }),
                getRandomChar(new char[] { 'A', 'B', 'C', 'D', 'E', 'F' }), faker.company().name(), getRandomLicence(),
                random.nextDouble(100), faker.country().name());
    }

    public static List<Integer> getTestScores(int amount) {
        List<Integer> scores = new ArrayList<>(amount);

        for (int i = 0; i < amount; i++) {
            scores.add(random.nextInt());
        }

        return scores;
    }

    public static Game getTestGameWithScores(int amountOfScores) {
        return new Game(getTestPlayer(), getTestScores(amountOfScores));
    }

}
