package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.games.Game;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    @Test
    void testIfMethodToSummarizeScoreWorks() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        testGame.addScore(150);
        testGame.addScore(50);

        assertEquals(200, testGame.getSum());
    }

    @Test
    void testIfMethodToCalculateMeanScoreValuesWorks() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        testGame.addScore(100);
        testGame.addScore(150);
        testGame.addScore(275);

        assertEquals(175.0, testGame.getMean());
    }

    @Test
    void testIfRemoveScoreFromIndexWorks() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        testGame.addScore(100);
        testGame.addScore(150);
        testGame.addScore(200);
        testGame.removeScore(2);

        assertEquals(2, testGame.getScores().size());
        assertEquals(250, testGame.getSum());
    }

    @Test
    void testThatNegativeValuesCannotBeAddedToScores() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        assertThrows(IllegalArgumentException.class, () -> testGame.addScore(-150));
    }

    @Test
    void testThatScoresOutsideOfIndexCannotBeRemoved() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        testGame.addScore(100);
        testGame.addScore(150);
        testGame.addScore(200);

        assertThrows(IndexOutOfBoundsException.class, () -> testGame.removeScore(-1));
    }

    @Test
    void testThatNumberOfScoresCannotBeEmpty() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        assertEquals(0, testGame.getSum());
    }

    @Test
    void testThatMeanScoreValueCannotBeLessThanZero() {
        Player testPlayer = TestData.getTestPlayer();
        Game testGame = new Game(testPlayer);

        assertEquals(0, testGame.getMean());
    }
}