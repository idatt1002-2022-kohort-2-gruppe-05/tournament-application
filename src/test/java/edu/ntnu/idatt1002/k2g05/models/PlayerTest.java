package edu.ntnu.idatt1002.k2g05.models;

import edu.ntnu.idatt1002.k2g05.models.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    @Test
    void testThatInvalidArgumentsForPlayerThrowsExceptions() {
        String validName = "Ola Nordmænn";
        String invalidName = "";
        char validGender = 'F';
        char invalidGender = 'H';
        char validPlayerClass = 'B';
        char invalidPlayerClass = 'l';
        String validClub = "Moss Bowling Klubb";
        String invalidClub = "";
        int validLicense = 25565;
        int invalidLicense = -1;
        double validHandicap = 2.5;
        double invalidHandicap = -2.76;
        String validCountry = "Norge";
        String invalidCountry = "";

        assertDoesNotThrow(() -> new Player(validName, validGender, validPlayerClass, validClub, validLicense,
                validHandicap, validCountry));

        assertThrows(IllegalArgumentException.class, () -> new Player(invalidName, validGender, validPlayerClass,
                validClub, validLicense, validHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, invalidGender, validPlayerClass,
                validClub, validLicense, validHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, validGender, invalidPlayerClass,
                validClub, validLicense, validHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, validGender, validPlayerClass,
                invalidClub, validLicense, validHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, validGender, validPlayerClass,
                validClub, invalidLicense, validHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, validGender, validPlayerClass,
                validClub, validLicense, invalidHandicap, validCountry));
        assertThrows(IllegalArgumentException.class, () -> new Player(validName, validGender, validPlayerClass,
                validClub, validLicense, validHandicap, invalidCountry));
    }
}