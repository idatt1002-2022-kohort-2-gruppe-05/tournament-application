package edu.ntnu.idatt1002.k2g05.factories;

import edu.ntnu.idatt1002.k2g05.models.Player;
import edu.ntnu.idatt1002.k2g05.models.RoundCollection;
import edu.ntnu.idatt1002.k2g05.models.TestData;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.QualificationRound;
import edu.ntnu.idatt1002.k2g05.models.rounds.Round;
import edu.ntnu.idatt1002.k2g05.models.rounds.RoundType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RoundCollectionFactoryTest {

    @Test
    void testUpdateGroupStageCollectionWithSnakeFill() {
        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("TestGroupStage", 4);
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < 32; i++) {
            players.add(new Player(Integer.toString(i), 'M', 'A', "TestClub", i, 0, "testCountry"));
        }

        RoundCollectionFactory.updateGroupStageCollectionWithSnakeFill(collection, players, 8);

        List<Object[]> names = new ArrayList<>();
        for (Round round : collection.getAllRounds()) {
            names.add(((MatchRound) round).getAllMatchGames().stream().map(MatchGame::getPlayer).map(Player::getName)
                    .toArray());

        }

        assertArrayEquals(new Object[] { "0", "7", "8", "15", "16", "23", "24", "31" }, names.get(0));
        assertArrayEquals(new Object[] { "1", "6", "9", "14", "17", "22", "25", "30" }, names.get(1));
        assertArrayEquals(new Object[] { "2", "5", "10", "13", "18", "21", "26", "29" }, names.get(2));
        assertArrayEquals(new Object[] { "3", "4", "11", "12", "19", "20", "27", "28" }, names.get(3));

    }

    @Test
    void testGetAndFillQualificationRoundCollection() {

        List<Player> testPlayers = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            Player testPlayer = TestData.getTestPlayer();
            testPlayers.add(testPlayer);
        }

        RoundCollection collection = RoundCollectionFactory
                .getAndFillQualificationRoundCollection("Test qualification round", testPlayers);

        assertEquals(RoundType.QUALIFICATION_ROUND, collection.getType());
        assertEquals(50, (((QualificationRound) collection.getAllRounds().get(0)).getAllGames().size()));
        assertEquals("Test qualification round", ((QualificationRound) collection.getAllRounds().get(0)).getName());

    }

    @Test
    void testIfGetAndFillQualificationRoundCollectionThowExeptionWhenNemIsEmpty() {

        List<Player> testPlayers = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            Player testPlayer = TestData.getTestPlayer();
            testPlayers.add(testPlayer);
        }

        assertThrows(IllegalArgumentException.class, () -> {
            RoundCollectionFactory.getAndFillQualificationRoundCollection("", testPlayers);
        });

        try {
            RoundCollectionFactory.getAndFillQualificationRoundCollection("", testPlayers);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Navnet på rundesamlingen kan ikke være tomt", e.getMessage());
        }
    }

    @Test
    void testGetEmptyGroupStageCollection() {
        RoundCollection roundCollection = RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 4);

        assertEquals(RoundType.GROUP_STAGE, roundCollection.getType());
        assertEquals(4, (roundCollection.getAllRounds().size()));
        assertEquals("Gruppe A", ((MatchRound) roundCollection.getAllRounds().get(0)).getName());
        assertFalse(((MatchRound) roundCollection.getAllRounds().get(0)).isFinal());
    }

    @Test
    void testIfGetEmptyGroupStageCollectionThrowsExeptionIfGrupusIsLessThanOne() {

        try {
            RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 0);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Antall grupper kan ikke være mindre enn en.", e.getMessage());
        }

        assertThrows(IllegalArgumentException.class, () -> {
            RoundCollectionFactory.getEmptyGroupStageCollection("test group stage", 0);
        });

    }

    @Test
    void testGetEmptyQuarterFinalCollection() {
        RoundCollection collection = RoundCollectionFactory.getEmptyQuarterFinalCollection("test kvartfianel");

        assertEquals(4, collection.getAllRounds().size());
        assertEquals("QF1", ((MatchRound) collection.getAllRounds().get(0)).getName());
        assertEquals("QF2", ((MatchRound) collection.getAllRounds().get(1)).getName());
        assertTrue(((MatchRound) collection.getAllRounds().get(1)).isFinal());
        assertEquals(RoundType.QUARTER_FINAL, collection.getType());
    }

    @Test
    void testGetEmptySemiFinalCollection() {
        RoundCollection collection = RoundCollectionFactory.getEmptySemiFinalCollection("test semifianle");

        assertEquals(2, collection.getAllRounds().size());
        assertEquals("SF1", ((MatchRound) collection.getAllRounds().get(0)).getName());
        assertEquals("SF2", ((MatchRound) collection.getAllRounds().get(1)).getName());
        assertTrue(((MatchRound) collection.getAllRounds().get(1)).isFinal());
        assertEquals(RoundType.SEMI_FINAL, collection.getType());
    }

    @Test
    void testGetEmptyFinalCollection() {
        RoundCollection collection = RoundCollectionFactory.getEmptyFinalCollection("test semifianle");

        assertEquals(1, collection.getAllRounds().size());
        assertEquals("F1", ((MatchRound) collection.getAllRounds().get(0)).getName());
        assertTrue(((MatchRound) collection.getAllRounds().get(0)).isFinal());
        assertEquals(RoundType.FINAL, collection.getType());
    }

    @Test
    void testGetEmptyQualificationCollection() {
        RoundCollection collection = RoundCollectionFactory.getEmptyQualificationCollection("test kvalifikasjon");

        assertEquals("Q1", collection.getAllRounds().get(0).getName());
        assertEquals(RoundType.QUALIFICATION_ROUND, collection.getType());
        assertEquals(1, collection.getAllRounds().size());
    }

    @Test
    void testFillQualificationCollection() {
        RoundCollection collection = RoundCollectionFactory.getEmptyQualificationCollection("test kvalifikasjon");
        List<Player> testPlayers = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            Player testPlayer = TestData.getTestPlayer();
            testPlayers.add(testPlayer);
        }

        RoundCollectionFactory.fillQualificationCollection(collection, testPlayers);

        assertEquals(RoundType.QUALIFICATION_ROUND, collection.getType());
        assertEquals(50, (((QualificationRound) collection.getAllRounds().get(0)).getAllGames().size()));
        assertEquals("Q1", ((QualificationRound) collection.getAllRounds().get(0)).getName());
    }

    @Test
    void testIfFillQualificationCollectionThrowsExeptionWhenParameterIsNotTypeQualification() {
        RoundCollection collection = RoundCollectionFactory.getEmptyGroupStageCollection("test gruppespill", 4);
        List<Player> testPlayers = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            Player testPlayer = TestData.getTestPlayer();
            testPlayers.add(testPlayer);
        }

        try {
            RoundCollectionFactory.fillQualificationCollection(collection, testPlayers);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "rundekolleksjonen du sendte inn er ikke en kvalifikasjons runde");
        }

        assertThrows(IllegalArgumentException.class, () -> {
            RoundCollectionFactory.fillQualificationCollection(collection, testPlayers);
        });
    }

}