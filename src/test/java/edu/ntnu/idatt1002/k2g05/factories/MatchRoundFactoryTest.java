package edu.ntnu.idatt1002.k2g05.factories;

import edu.ntnu.idatt1002.k2g05.models.TestData;
import edu.ntnu.idatt1002.k2g05.models.games.MatchGame;
import edu.ntnu.idatt1002.k2g05.models.rounds.MatchRound;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatchRoundFactoryTest {

    @Test
    void testUpdateMatchesInMatchRoundWithRoundRobin() {
        MatchRound matchRound = new MatchRound("Test Group");
        for (int i = 0; i < 8; i++) {
            matchRound.addMatchGame(new MatchGame(TestData.getTestPlayer()));
        }

        MatchRoundFactory.fillMatchesInMatchRoundWithRoundRobin(matchRound);

        for (MatchGame matchGame : matchRound.getAllMatchGames()) {
            assertEquals(7, matchGame.getAllMatches().size());
        }
    }
}